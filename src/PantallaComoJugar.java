/**
	Nombre del proyecto:
	Sudoku Microedition

	Descripci�n de la clase:
	Esta pantalla muestra una breve introducci�n a la forma de jugar con esta 
	aplicaci�n.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import javax.microedition.lcdui.*;

public class PantallaComoJugar extends Form implements CommandListener, Pantalla {
	public PantallaComoJugar() {
		super("�C�mo se juega?");
		
		append(new StringItem(
			"Introducci�n",
			"Es un rompecabezas matem�tico japon�s que empez� a popularizarse en 1986 y salt� al �mbito internacional en 2005."
			));
			
		append(new StringItem(
			"�C�mo se juega?",
			"Este juego consiste en un tablero de 9x9 casillas donde cada fila y cada columna debe de contener n�meros del 1 al 9 sin repetir."
			));
		append(new StringItem(
			null,
			"Adem�s, el tablero de 9x9 casillas tambi�n hay est� dispuesto por secci�nes de 3x3 casillas donde cada secci�n debe de contener los n�meros del 1 al 9 sin repetir."
			));
			
		append(new StringItem(
			"Objetivo",
			"Sabiendo c�mo est� constituido el Sudoku, algunas casillas se han eliminado y el objetivo del juego es adivinar qu� n�mero contienen las casillas eliminadas."
			));
			
		append(new StringItem(
			"Manejo de esta aplicaci�n",
			"Durante el juego, podr�s usar los botones de direcci�n para mover el cursor y las teclas del 1 al 9 para poner n�mero en el hueco."
			));
		append(new StringItem(
			null,
			"La tecla del 0 sirve para dejar el hueco otra vez vac�o."
			));
			
		append(new StringItem(
			"M�s ayuda",
			"Podr�s encontrar m�s informaci�n desde el apartado 'Ayuda' durante el juego."
			));
			
		append(new StringItem(
			"Notas",
			"Se ha detectado que con algunos m�viles, al inicial el juego, a veces, se ve parada pero entrando en 'Opciones' y luego volviendo al juego se soluciona."
			));
		
		addCommand(new Command("Volver",Command.EXIT,1));
		setCommandListener(this);
	}
	public void activarPantalla() {
	}
	public void commandAction(Command c, Displayable d) {
		SudokuME.gestionPantallas.pantallaCerrada();
	}
}