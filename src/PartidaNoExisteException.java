public class PartidaNoExisteException extends Exception {
	public PartidaNoExisteException() {
		super("Partida no encontrada");
	}
}