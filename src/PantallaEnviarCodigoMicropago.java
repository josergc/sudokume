/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	En esta pantalla se env�a autom�ticamente el c�digo recibido para hacer 
	efectivo el micropago.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.wireless.messaging.*;

public class PantallaEnviarCodigoMicropago extends Form implements Runnable, Pantalla {
	protected String codigoAEnviar;

	public PantallaEnviarCodigoMicropago(String codigoAEnviar) {
		super("Enviando...");
		
		this.codigoAEnviar = codigoAEnviar;
		
		append(new StringItem("Enviando c�digo","Espere por favor..."));
	}
	
	public void run() {
		// Env�a el mensaje
		synchronized (this) {
			HttpConnection conexion = null;
			InputStream is = null;
			try {
				conexion = (HttpConnection)Connector.open(
					"http://69.36.9.147:8090/clientes/SMS_API_OUT.jsp?cliente=SEQWR&codigo=" + codigoAEnviar
					);
				int rc = conexion.getResponseCode();
				if (rc != HttpConnection.HTTP_OK) {
					SudokuME.gestionPantallas.ponPantalla(new PantallaError(
						"Error al enviar una consulta",
						"Recibido el c�digo HTTP " + rc + ".\nPuede volverlo a intentar m�s tarde, o bien contactar conmigo en josergc@lycos.es o bien visitar la web www.josergc.tk"
						));
				} else {
					is = conexion.openInputStream();
					String cadenaRecibida = new String(Utiles.flujoByteArray(is));
					SudokuME.gestionPantallas.pantallaCerrada();
					SudokuME.gestionPantallas.ponPantalla(new PantallaMensaje(
						"Recibido '" + cadenaRecibida + "'",
						"1.- C�digo v�lido sin usar\n2.- C�digo v�lido pero usado\n3.- C�digo no v�lido\n4.- Error interno"
						));
				}
			} catch(IOException e) {
				SudokuME.gestionPantallas.pantallaCerrada();
				SudokuME.gestionPantallas.ponPantalla(new PantallaError(
					"Error de entrada/salida",
					"No se ha podido realizar la operaci�n.\n" + e.toString() + "\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk"
					));
			} catch(ClassCastException e) {
				SudokuME.gestionPantallas.pantallaCerrada();
				SudokuME.gestionPantallas.ponPantalla(new PantallaError(
					"No se puede realizar la acci�n",
					"Este dispositivo m�vil no est� capacitado para hacer conexiones HTTP.\nP�ngase en contacto conmigo por el e-mail josergc@lycos.es o por la p�gina web www.josergc.tk"
					));
			} finally {
				try { if (is != null) is.close(); } catch(IOException e) { }
				try { if (conexion != null) conexion.close(); } catch(IOException e) { }
			}
		}
	}
	public void activarPantalla() {
	}
}