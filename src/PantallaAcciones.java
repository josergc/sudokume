/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta pantalla muestra una lista con las opciones que se pueden hacer en el 
	tablero como poner o quitar un n�mero.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaAcciones extends Lista implements CommandListener, Pantalla {
	public static final int CMD_QUITAR_NUMERO = 0;
	public static final int CMD_PONER_1 = 1;
	public static final int CMD_PONER_2 = 2;
	public static final int CMD_PONER_3 = 3;
	public static final int CMD_PONER_4 = 4;
	public static final int CMD_PONER_5 = 5;
	public static final int CMD_PONER_6 = 6;
	public static final int CMD_PONER_7 = 7;
	public static final int CMD_PONER_8 = 8;
	public static final int CMD_PONER_9 = 9;
	public static final int CMD_VOLVER = 10;
	protected int ultimoComando;

	protected static final String[] opciones = {
		"Quitar n�mero",
		"Poner un 1",
		"Poner un 2",
		"Poner un 3",
		"Poner un 4",
		"Poner un 5",
		"Poner un 6",
		"Poner un 7",
		"Poner un 8",
		"Poner un 9",
		"Volver"
		};
	protected static Image[] imagenes = null;
	
	public PantallaAcciones() {
		super("Acciones",List.IMPLICIT);
		if (imagenes == null) {
			try {
				imagenes = new Image[] {
					Image.createImage("/limpiarCasilla.png"),
					Image.createImage("/amarillo1.png"),
					Image.createImage("/amarillo2.png"),
					Image.createImage("/amarillo3.png"),
					Image.createImage("/amarillo4.png"),
					Image.createImage("/amarillo5.png"),
					Image.createImage("/amarillo6.png"),
					Image.createImage("/amarillo7.png"),
					Image.createImage("/amarillo8.png"),
					Image.createImage("/amarillo9.png"),
					Image.createImage("/volver.png")
					};
			} catch(IOException e) {
				imagenes = new Image[opciones.length];
				for (int i = 0; i < imagenes.length; i++)
					imagenes[i] = null;
			}
		}
		for (int i = 0; i < opciones.length; i++) 
			append(opciones[i],imagenes[i]);
			
		setCommandListener(this);
	}
	
	public void activarPantalla() {
	}
	
	public void commandAction(Command c, Displayable d) {
		if (c == SELECT_COMMAND) {
			ultimoComando = getSelectedIndex();
			if (ultimoComando == -1) ultimoComando = CMD_VOLVER;
			SudokuME.gestionPantallas.pantallaCerrada();
		}
	}
	
	public int devUltimoComando() {
		return ultimoComando;
	}
}