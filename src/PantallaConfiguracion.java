/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase representa la pantalla de configuraci�n del juego global 
	donde podemos indicar el nivel de dificultad, si activamos la ayuda 
	durante el juego, si queremos el juego con tiempo, y activar y 
	desactivar la m�sica y los sonidos as� como decir el volumen de estos.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import javax.microedition.lcdui.*;

public class PantallaConfiguracion extends Form implements CommandListener, Pantalla {
	protected Command cmdAceptar;
	protected Command cmdCancelar;
	
	protected Gauge dificultadSudoku = new Gauge("Nivel de dificultad",true,4,0);
	protected ChoiceGroup sudokuConAyuda = new ChoiceGroup("Ayuda activada",ChoiceGroup.EXCLUSIVE,new String[]{"S�","No"},null);
	protected TextField sudokuConTiempo = new TextField("Tiempo en min. (0 = desactivado)","0",3,TextField.NUMERIC);
	protected ChoiceGroup activarMusica = new ChoiceGroup("Activar m�sica",ChoiceGroup.EXCLUSIVE,new String[]{"S�","No"},null);
	protected Gauge volumenMusica = new Gauge("Volumen m�sica",true,5,4);
	protected ChoiceGroup activarSonidos = new ChoiceGroup("Activar sonidos",ChoiceGroup.EXCLUSIVE,new String[]{"S�","No"},null);
	protected Gauge volumenSonidos = new Gauge("Volumen sonidos",true,5,4);

	public PantallaConfiguracion() {
		super("Configuraci�n");
		
		append(dificultadSudoku);
		append(sudokuConAyuda);
		append(sudokuConTiempo);
		append(activarMusica);
		append(volumenMusica);
		append(activarSonidos);
		append(volumenSonidos);
		
		addCommand(cmdAceptar = new Command("Aceptar",Command.OK,1));
		addCommand(cmdCancelar = new Command("Cancelar",Command.CANCEL,1));
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		if (c == cmdAceptar) {
			// Guarda la configuraci�n seg�n los datos del formulario
			SudokuME.configuracion.dificultadSudoku = dificultadSudoku.getValue();
			SudokuME.configuracion.sudokuConAyuda = sudokuConAyuda.getSelectedIndex() == 0;
			SudokuME.configuracion.sudokuConTiempo = sudokuConTiempo.getString().compareTo("") == 0 ? 0 : (Long.parseLong(sudokuConTiempo.getString()) * 60 * 1000);
			SudokuME.configuracion.activarMusica = activarMusica.getSelectedIndex() == 0;
			SudokuME.configuracion.volumenMusica = volumenMusica.getValue() * 16;
			SudokuME.configuracion.activarSonidos = activarSonidos.getSelectedIndex() == 0;
			SudokuME.configuracion.volumenSonidos = volumenSonidos.getValue() * 16;
			try { 
				SudokuME.gestionJuego.guardarConfiguracion();
				SudokuME.controlSonido(SudokuME.configuracion.activarMusica);
				SudokuME.gestionPantallas.pantallaCerrada();
			} catch (Exception e) {
				SudokuME.gestionPantallas.ponPantalla(
					new PantallaError("No se ha podido guardar la configuraci�n",e.toString())
					);
			}
		} else 
			SudokuME.gestionPantallas.pantallaCerrada();
	}
	public void activarPantalla() {
		// Carga la configuraci�n en el formulario
		dificultadSudoku.setValue(SudokuME.configuracion.dificultadSudoku);
		sudokuConAyuda.setSelectedIndex(SudokuME.configuracion.sudokuConAyuda ? 0 : 1,true);
		sudokuConTiempo.setString(Long.toString(SudokuME.configuracion.sudokuConTiempo / (1000 * 60)));
		activarMusica.setSelectedIndex(SudokuME.configuracion.activarMusica ? 0 : 1,true);
		volumenMusica.setValue(SudokuME.configuracion.volumenMusica / 16);
		activarSonidos.setSelectedIndex(SudokuME.configuracion.activarSonidos ? 0 : 1,true);
		volumenSonidos.setValue(SudokuME.configuracion.volumenSonidos / 16);
	}
}