/**
	Nombre del proyecto:
	Sudoku Microedition

	Descripci�n de la clase:
	Esta pantalla muestra los datos acerca del juego
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import javax.microedition.lcdui.*;

public class PantallaAcercaDe extends Form implements CommandListener, Pantalla {
	public PantallaAcercaDe() {
		super("Acerca de...");
		
		append(new StringItem("Aplicaci�n","Sudoku Microedition (SudokuME)"));
		append(new StringItem("Versi�n","1.0"));
		append(new StringItem("Fecha","29 de mayo de 2.006"));
		append(new StringItem("Autor","Jos� Roberto Garc�a Chico"));
		append(new StringItem("e-mail","josergc@lycos.es"));
		append(new StringItem("Web","www.josergc.tk"));
		append(new StringItem("Licencia","Esto es software libre seg�n la licencia de copyleft GNU/GPL versi�n 2"));
		
		addCommand(new Command("Volver",Command.EXIT,1));
		setCommandListener(this);
	}
	public void activarPantalla() {
	}
	public void commandAction(Command c, Displayable d) {
		SudokuME.gestionPantallas.pantallaCerrada();
	}
}