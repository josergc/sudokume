/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Pantalla presentaci�n del juego donde se puede iniciar una nueva partida, 
	cargar una partida, configurar el juego y algunas cosas m�s. Pone un 
	mensaje de iniciando cuando se elige jugar un juego nuevo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.util.*;
import javax.microedition.lcdui.*;

public class PantallaPresentacion extends Canvas implements CommandListener, Runnable, Pantalla {
	protected final static String cadenaIniciando = "Iniciando...";
	protected final static int TOPLEFT = Graphics.TOP | Graphics.LEFT;
	protected final static int TOPHCENTER = Graphics.TOP | Graphics.HCENTER;

	protected boolean pantallaIniciada;
	
	protected Image imgFondo;
	protected int nFilasFondo;
	protected int nColumnasFondo;
	protected int xFondo;
	protected int yFondo;
	protected int vxFondo;
	protected int vyFondo;
	
	protected boolean iniciandoJuego;
	protected int dibujaCadenaIniciando;
	protected boolean cadenaIniciandoNoDibujada;
	
	protected Image imgSudoku = null;
	protected int xImgSudoku;
	protected int yImgSudoku;
	protected int vxImgSudoku;
	protected int vyImgSudoku;
	
	protected static Command cmdJugar = new Command("�JUGAR!",Command.ITEM,1);
	protected static Command cmdCargar = new Command("Cargar",Command.ITEM,1);
	protected static Command cmdConfigurar = new Command("Configurar",Command.ITEM,1);
	protected static Command cmdComoJugar = new Command("�C�mo se juega?",Command.ITEM,1);
	protected static Command cmdHacerDonativo = new Command("Haz un donativo",Command.ITEM,1);
	protected static Command cmdAcercaDe = new Command("Acerca de...",Command.ITEM,1);
	protected static Command cmdSalir = new Command("Salir",Command.EXIT,1);
	
	protected boolean cerrarPantalla = false;
	
	public final static int CMD_NADA = -1;
	public final static int CMD_JUGAR = 0;
	public final static int CMD_CARGAR = 1;
	public final static int CMD_CONFIGURAR = 2;
	public final static int CMD_COMO_JUGAR = 3;
	public final static int CMD_HACER_DONATIVO = 4;
	public final static int CMD_ACERCA_DE = 5;
	public final static int CMD_SALIR = 6;
	protected int ultimoComando = CMD_NADA;
	
	public PantallaPresentacion() {
		super();

		iniciandoJuego = false;
		cadenaIniciandoNoDibujada = true;
		
		pantallaIniciada = false;
		
		addCommand(cmdJugar);
		addCommand(cmdCargar);
		addCommand(cmdConfigurar);
		addCommand(cmdComoJugar);
		addCommand(cmdHacerDonativo);
		addCommand(cmdAcercaDe);
		addCommand(cmdSalir);
		setCommandListener(this);

	}
	
	protected void iniciaFondo() {
		nFilasFondo = getHeight() / SudokuME.tamCasilla;
		nFilasFondo += (nFilasFondo * SudokuME.tamCasilla < getHeight()) ? 2 : 1;
		nColumnasFondo = getWidth() / SudokuME.tamCasilla;
		nColumnasFondo += (nColumnasFondo * SudokuME.tamCasilla < getWidth()) ? 2 : 1;
		imgFondo = Image.createImage(
			nColumnasFondo * SudokuME.tamCasilla,
			nFilasFondo * SudokuME.tamCasilla
			);
	}
	
	protected Image devCasillaParaDibujarFondo() {
		switch(Math.abs(Constantes.r.nextInt()) % 4) {
			case 0:
				return SudokuME.numerosAmarillos[
					Math.abs(Constantes.r.nextInt()) % SudokuME.numerosAmarillos.length
					];
			case 1:
				return SudokuME.numerosAzules[
					Math.abs(Constantes.r.nextInt()) % SudokuME.numerosAzules.length
					];
			case 2:
				return SudokuME.numerosRojos[
					Math.abs(Constantes.r.nextInt()) % SudokuME.numerosRojos.length
					];
			default:
				return SudokuME.numerosBlancos[
					Math.abs(Constantes.r.nextInt()) % SudokuME.numerosBlancos.length
					];
		}
	}
	
	protected void rellenaFondo() {
		int i, y, j, x;
		Graphics gFondo = imgFondo.getGraphics();
		for (i = 0, y = 0; i < nFilasFondo; i++, y += SudokuME.tamCasilla)
			for (j = 0, x = 0; j < nColumnasFondo; j++, x += SudokuME.tamCasilla) {
				gFondo.drawImage(
					devCasillaParaDibujarFondo(),
					x,
					y,
					TOPLEFT
					);
			}
	}
	
	protected void redibujaFondo() {
		int i, f, x, y;
		Image nuevoFondo = Image.createImage(
			nColumnasFondo * SudokuME.tamCasilla,
			nFilasFondo * SudokuME.tamCasilla
			);
		Graphics gNuevoFondo = nuevoFondo.getGraphics();
		gNuevoFondo.drawImage(
			imgFondo,
			-SudokuME.tamCasilla,
			-SudokuME.tamCasilla,
			TOPLEFT
			);
		for (i = 0, x = imgFondo.getWidth() - SudokuME.tamCasilla, y = 0; i < nFilasFondo; i++, y += SudokuME.tamCasilla)
			gNuevoFondo.drawImage(
				devCasillaParaDibujarFondo(),
				x,
				y,
				TOPLEFT
				);
		f = nColumnasFondo - 1;
		for (i = 0, x = 0, y = imgFondo.getHeight() - SudokuME.tamCasilla; i < f; i++, x += SudokuME.tamCasilla)
			gNuevoFondo.drawImage(
				devCasillaParaDibujarFondo(),
				x,
				y,
				TOPLEFT
				);
		imgFondo = nuevoFondo;
	}
	
	public void paint(Graphics g) {
		if (iniciandoJuego) {
			if ((dibujaCadenaIniciando & 3) != 0) {
				if (cadenaIniciandoNoDibujada) {
					g.setColor(0);
					g.fillRect(0,0,Constantes.anchoPantalla,Constantes.altoPantalla);
					g.setColor(0xffffff);
					g.drawString(
						cadenaIniciando,
						Constantes.anchoPantalla >> 1,
						(Constantes.altoPantalla - Constantes.f.getHeight()) / 2,
						TOPHCENTER
						);
					cadenaIniciandoNoDibujada = false;
				} 
			} else {
				g.setColor(0);
				g.fillRect(0,0,Constantes.anchoPantalla,Constantes.altoPantalla);
				cadenaIniciandoNoDibujada = true;
			}
		} else {
			// Dibuja el fondo de la pantalla
			g.drawImage(imgFondo,xFondo,yFondo,TOPLEFT);
			// Dibuja el t�tulo del juego
			g.drawImage(imgSudoku,xImgSudoku,yImgSudoku,TOPLEFT);
		}
	}
	
	public void run() {
		if (iniciandoJuego) {
			dibujaCadenaIniciando++;
		} else {
			if (xImgSudoku + vxImgSudoku + imgSudoku.getWidth() >= Constantes.anchoPantalla)
				vxImgSudoku = -(1 + Math.abs(Constantes.r.nextInt() % 4));
			if (yImgSudoku + vyImgSudoku + imgSudoku.getHeight() >= Constantes.altoPantalla)
				vyImgSudoku = -(1 + Math.abs(Constantes.r.nextInt() % 4));
			if (xImgSudoku + vxImgSudoku < 0)
				vxImgSudoku = 1 + Math.abs(Constantes.r.nextInt() % 4);
			if (yImgSudoku + vyImgSudoku < 0)
				vyImgSudoku = 1 + Math.abs(Constantes.r.nextInt() % 4);
			
			xImgSudoku += vxImgSudoku;
			yImgSudoku += vyImgSudoku;
		
			if (xFondo + vxFondo < -SudokuME.tamCasilla) {
				redibujaFondo();
				xFondo = 0;
				yFondo = 0;
			}
			xFondo += vxFondo;
			yFondo += vyFondo;
		}
		
		repaint();
		
		try { Thread.sleep(50); } catch (InterruptedException e) {}
		
		if (SudokuME.gestionPantallas.devPantallaActual() == this) {
			if (cerrarPantalla)
				SudokuME.gestionPantallas.pantallaCerrada();
			else {
				Constantes.d.callSerially(this);
				SudokuME.controlSonido(SudokuME.melodiaMenu);
			}
		}
	}
	
	public void activarPantalla() {
		if (pantallaIniciada) {
			
			iniciandoJuego = false;
			
			if (ultimoComando == CMD_CARGAR) {
				if (SudokuME.pantallaPartidasSinOpcionNuevo.devUltimoComando() == PantallaPartidas.CMD_CARGAR) {
					SudokuME.pantallaJuego = new PantallaJuego(
						SudokuME.configuracion.sudokuConAyuda,
						SudokuME.configuracion.sudokuConTiempo
						);
					SudokuME.pantallaJuego.juegoCargado();
					SudokuME.gestionPantallas.ponPantalla(
						SudokuME.pantallaJuego
						);
				}
			}
		} else {
			xImgSudoku = 0;
			yImgSudoku = 0;
			vxImgSudoku = 1 + Math.abs(Constantes.r.nextInt() % 4);
			vyImgSudoku = 1 + Math.abs(Constantes.r.nextInt() % 4);
			try {
				imgSudoku = Image.createImage("/sudoku" + (Constantes.r.nextInt() & 3) + ".png");
			} catch(Exception e) {
				imgSudoku = null;
			}
		
			xFondo = 0;
			yFondo = 0;
			vxFondo = -1;
			vyFondo = -1;
			if (imgFondo == null) {
				iniciaFondo();
				rellenaFondo();
			}
		
			dibujaCadenaIniciando = 0;
			
			pantallaIniciada = true;
		}
	}
	
	public void commandAction(Command c, Displayable d) {
		if (iniciandoJuego) 
			return;
		if (c == cmdJugar) {
			ultimoComando = CMD_JUGAR;
			iniciaJuego();
		} else if (c == cmdCargar) {
			ultimoComando = CMD_CARGAR;
			SudokuME.gestionPantallas.ponPantalla(SudokuME.pantallaPartidasSinOpcionNuevo);
		} else if (c == cmdConfigurar) {
			ultimoComando = CMD_CONFIGURAR;
			SudokuME.gestionPantallas.ponPantalla(SudokuME.pantallaConfiguracion);
		} else if (c == cmdComoJugar) {
			ultimoComando = CMD_COMO_JUGAR;
			SudokuME.gestionPantallas.ponPantalla(new PantallaComoJugar());
		} else if (c == cmdHacerDonativo) {
			ultimoComando = CMD_HACER_DONATIVO;
			SudokuME.gestionPantallas.ponPantalla(new PantallaHacerDonativo());
		} else if (c == cmdAcercaDe) {
			ultimoComando = CMD_ACERCA_DE;
			SudokuME.gestionPantallas.ponPantalla(new PantallaAcercaDe());
		} else if (c == cmdSalir) {
			ultimoComando = CMD_SALIR;
			cerrarPantalla = true;
		}
	}
	
	protected void iniciaJuego() {
		iniciandoJuego = true;
		new Thread(new HiloIniciaJuego()).start();
	}
	
	public void keyPressed(int keyCode) {
		if (iniciandoJuego) 
			return;
		int a = getGameAction(keyCode);
		if (a == Constantes.botonDisparo) {
			ultimoComando = CMD_JUGAR;
			iniciaJuego();
		}
	}
}