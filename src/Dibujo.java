import java.util.*;

public class Dibujo {
	final static int 
		TIPO_RGBPALETA = 0,
		TIPO_VECTOR = 1,
		TIPO_RGB = 2,
		TIPO_VECTORIAL = 3;
	public static int[] paleta = null;
	public int[] paletaPropia = null;
	public int tipo = TIPO_RGBPALETA;
	public int ancho = 0;
	public int alto = 0;
	public int anchoEscala = 0;
	public int altoEscala = 0;
	public int[] buffer = null;
	public byte[] bufferPaleta = null;
	public Vector v = new Vector();
	public Dibujo() {
		if (paleta == null) {
			reiniciaPaleta();
		}
	}
	public Dibujo(Dibujo d) {
		if (paleta == null) {
			reiniciaPaleta();
		}
		if (d.paletaPropia != null) {
			paletaPropia = new int[d.paletaPropia.length];
			for (int i = d.paletaPropia.length - 1; i >= 0; i--)
				paletaPropia[i] = d.paletaPropia[i];
		}
		tipo = d.tipo;
		ancho = d.ancho;
		alto = d.alto;
		anchoEscala = d.anchoEscala;
		altoEscala = d.altoEscala;
		if (d.buffer != null) {
			buffer = new int[d.buffer.length];
			for (int i = buffer.length - 1; i >= 0; i--)
				buffer[i] = d.buffer[i];
		}
		if (d.bufferPaleta != null) {
			bufferPaleta = new byte[d.bufferPaleta.length];
			for (int i = bufferPaleta.length - 1; i >= 0; i--)
				bufferPaleta[i] = d.bufferPaleta[i];
		}
		Enumeration e = d.v.elements();
		while (e.hasMoreElements())
			v.addElement(new DibujoRect((DibujoRect)e.nextElement()));
	}
	public void vaciaBuffer() {
		v.removeAllElements();
		for (int i = buffer.length - 1; i >= 0; i--)
			buffer[i] = -1;
		for (int i = bufferPaleta.length - 1; i >= 0; i--)
			bufferPaleta[i] = -1;
	}
	public void dibujaRectRelleno(int x, int y, int ancho, int alto, int colorRGB) {
		/*int x0, d = y * this.ancho + x, b = ;
		int xFinal = x + ancho;
		int yFinal = y + alto;
		for (; y < yFinal; y++)
			for (x0 = x; x0 < xFinal; x0++)
				buffer[
				*/
	}
	public void dibujaRectRelleno(DibujoRect dr) {
		dibujaRectRelleno(dr.x,dr.y,dr.ancho,dr.alto,dr.color);
	}
	public static void reiniciaPaleta() {
		paleta = new int[64];
		int r, g, b, c, R, RG; 
		for (r = 0, c = 0; r <= 3; r++) {
			R = ((r << 6) | (r << 4) | (r << 2) | r) << 16;
			for (g = 0; g <= 3; g++) {
				RG = R | (((g << 6) | (g << 4) | (g << 2) | g) << 8);
				for (b = 0; b <= 3; b ++, c++)
					paleta[c] = RG | ((b << 6) | (b << 4) | (b << 2) | b);
			}
		}
	}
	public static byte rgb2paleta(int colorRGB) {
		int r = (colorRGB >> 22) & 0x3;
		int g = (colorRGB >> 14) & 0x3;
		int b = (colorRGB >>  6) & 0x3;
		return (byte)((r << 4) | (g << 2) | b);
	}
	public void creaPaletaPropia() {
		paletaPropia = new int[paleta.length];
		for (int i = paleta.length - 1; i >= 0; i--)
			paletaPropia[i] = paleta[i];
	}
	public void espejoHorizontal() {
		if (tipo == TIPO_VECTOR) {
		} else if (tipo == TIPO_RGB) {
			int[] bufferAux = new int[buffer.length];
			int x, y, c, d, e;
			for (y = 0, c = 0, d = 0; y < alto; y++, d += ancho) 
				for (x = 0, e = ancho - 1; x < ancho; x++, c++, e--) 
					bufferAux[d + e] = buffer[c];
			buffer = bufferAux;
		} else if (tipo == TIPO_RGBPALETA) {
			byte[] bufferAux = new byte[bufferPaleta.length];
			int x, y, c, d, e;
			for (y = 0, c = 0, d = 0; y < alto; y++, d += ancho) 
				for (x = 0, e = ancho - 1; x < ancho; x++, c++, e--) 
					bufferAux[d + e] = bufferPaleta[c];
			bufferPaleta = bufferAux;
		}
	}
	final static int precision = 8;
	final static int _1 = 1 << precision;
	public static DibujoRect[] escala(Dibujo d, int nuevoAncho, int nuevoAlto) {
		DibujoRect[] dr;
		Vector v = new Vector();
		if (d.tipo == TIPO_VECTOR) {
			Enumeration e = d.v.elements();
			while (e.hasMoreElements()) 
				v.addElement(new DibujoRect((DibujoRect)e.nextElement()));
		} else if (d.tipo == TIPO_RGB) {
			int c;
			int x, x1, x11, tx1, y, y1, y11, Y, ty1;
			int xFinal = d.ancho << precision;
			int xFinal1 = nuevoAncho << precision;
			int yFinal = d.alto << precision;
			int yFinal1 = nuevoAlto << precision;
			int dx, dx1, dy, dy1, DY;
			if (nuevoAncho >= d.ancho) {
				dx = _1;
				dx1 = xFinal1 / d.ancho;
			} else {
				dx = xFinal / nuevoAncho;
				dx1 = _1;
			}
			if (nuevoAlto >= d.alto) {
				dy = _1;
				dy1 = yFinal1 / d.alto;
			} else {
				dy = yFinal / nuevoAlto;
				dy1 = _1;
			}
			DY = dy * d.ancho;
			for (
				y = 0, y1 = 0, Y = 0, y11 = dy1; 
				y < yFinal; 
				y += dy, y1 += dy1, Y += DY, y11 += dy1
				) {
				for (
					x = 0, x1 = 0, x11 = dx1; 
					x < xFinal; 
					x += dx, x1 += dx1, x11 += dx1
					) {
					c = (x + Y) >> precision;
					if (d.buffer[c] != -1) 
						v.addElement(new DibujoRect(
							tx1 = (x1 >> precision),
							ty1 = (y1 >> precision),
							(x11 >> precision) - tx1,
							(y11 >> precision) - ty1,
							d.buffer[c]
							));
				}
			}
		} else if (d.tipo == TIPO_RGBPALETA) {
			int c;
			int x, x1, x11, tx1, y, y1, y11, Y, ty1;
			int xFinal = d.ancho << precision;
			int xFinal1 = nuevoAncho << precision;
			int yFinal = d.alto << precision;
			int yFinal1 = nuevoAlto << precision;
			int dx, dx1, dy, dy1, DY;
			if (nuevoAncho >= d.ancho) {
				dx = _1;
				dx1 = xFinal1 / d.ancho;
			} else {
				dx = xFinal / nuevoAncho;
				dx1 = _1;
			}
			if (nuevoAlto >= d.alto) {
				dy = _1;
				dy1 = yFinal1 / d.alto;
			} else {
				dy = yFinal / nuevoAlto;
				dy1 = _1;
			}
			DY = dy * d.ancho;
			for (
				y = 0, y1 = 0, Y = 0, y11 = dy1; 
				y < yFinal; 
				y += dy, y1 += dy1, Y += DY, y11 += dy1
				) {
				for (
					x = 0, x1 = 0, x11 = dx1; 
					x < xFinal; 
					x += dx, x1 += dx1, x11 += dx1
					) {
					c = (x + Y) >> precision;
					if (d.bufferPaleta[c] != -1) 
						v.addElement(new DibujoRect(
							tx1 = (x1 >> precision),
							ty1 = (y1 >> precision),
							(x11 >> precision) - tx1,
							(y11 >> precision) - ty1,
							d.bufferPaleta[c]
							));
				}
			}
		}
		dr = new DibujoRect[v.size()];
		Enumeration e = v.elements();
		for (int dri = 0; e.hasMoreElements(); dri++)
			dr[dri] = (DibujoRect)e.nextElement();
		return dr;
	}
	public static Dibujo Vector2RGB(Dibujo d) {
		if (d.tipo == TIPO_VECTOR) {
			Dibujo b = new Dibujo(d);
			b.tipo = TIPO_RGB;
			b.buffer = new int[b.ancho * b.alto];
			DibujoRect dr;
			Enumeration e = d.v.elements();
			b.vaciaBuffer();
			while (e.hasMoreElements())
				b.dibujaRectRelleno((DibujoRect)e.nextElement());
			return b;
		} 
		return null;
	}
}