import java.io.*;
import java.util.*;
import javax.microedition.rms.*;

public class GestionJuego {
	protected RecordStore rsPartidas;
	protected RecordStore rsPuntuaciones;
	protected RecordStore rsConfiguracion;
	
	public GestionJuego() throws
		RecordStoreException
		{
		rsPartidas = RecordStore.openRecordStore("SudokuME.partidas",true);
		rsPuntuaciones = RecordStore.openRecordStore("SudokuME.puntuaciones",true);
		rsConfiguracion = RecordStore.openRecordStore("SudokuME.configuracion",true);
		
	}
	public void guardarConfiguracion() throws 
		RecordStoreNotOpenException, 
		InvalidRecordIDException,
		RecordStoreException, 
		RecordStoreFullException,
		IOException
		{
		byte[] ba;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		SudokuME.configuracion.guardar(baos);
		ba = baos.toByteArray();
		if (rsConfiguracion.getNumRecords() == 0) 
			rsConfiguracion.addRecord(ba,0,ba.length);
		else {
			RecordEnumeration re = rsConfiguracion.enumerateRecords(null,null,false);
			rsConfiguracion.setRecord(re.nextRecordId(),ba,0,ba.length);
		}
	}
	public void cargarConfiguracion() throws 
		RecordStoreNotOpenException, 
		InvalidRecordIDException,
		RecordStoreException, 
		IOException
		{
		if (rsConfiguracion.getNumRecords() != 0) {
			RecordEnumeration re = rsConfiguracion.enumerateRecords(null,null,false);
			SudokuME.configuracion.cargar(
				new ByteArrayInputStream(rsConfiguracion.getRecord(re.nextRecordId()))
				);
		}
	}
	public Vector listaPartidas() throws 
		RecordStoreNotOpenException, 
		InvalidRecordIDException,
		RecordStoreException, 
		IOException,
		PartidaNoExisteException
		{
		Vector nombres = new Vector();
		RecordEnumeration re = rsPartidas.enumerateRecords(null,null,false);
		DataInputStream dis;

		while (re.hasNextElement()) {
			dis = new DataInputStream(new ByteArrayInputStream(re.nextRecord()));
			nombres.addElement(dis.readUTF());
		}
		return nombres;
	}
	public void guardarPartida(String nombre, Sudoku sudoku) throws
		RecordStoreNotOpenException, 
		InvalidRecordIDException,
		RecordStoreException, 
		RecordStoreFullException,
		IOException
		{
		byte[] ba = creaByteArray(nombre,sudoku);
		RecordEnumeration re = rsPartidas.enumerateRecords(null,null,false);
		DataInputStream dis;
		int id;
		
		// Si el nombre existe, sobreescribe
		while (re.hasNextElement()) {
			id = re.nextRecordId(); 
			dis = new DataInputStream(new ByteArrayInputStream(rsPartidas.getRecord(id)));
			if (dis.readUTF().compareTo(nombre) == 0) {
				rsPartidas.setRecord(id,ba,0,ba.length);
				return;
			}
		}
		
		// Si el nombre no existe, crea una nueva entrada
		rsPartidas.addRecord(ba,0,ba.length);
	}
	protected byte[] creaByteArray(String nombre, Sudoku sudoku) throws
		IOException
		{
		ByteArrayOutputStream baosSudoku = new ByteArrayOutputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		byte[] ba;
		sudoku.guardar(baosSudoku);
		ba = baosSudoku.toByteArray();
		dos.writeUTF(nombre);
		dos.writeInt(ba.length);
		dos.write(ba,0,ba.length);
		return baos.toByteArray();
	}
	public void cargarPartida(String nombre, Sudoku sudoku) throws
		RecordStoreNotOpenException, 
		InvalidRecordIDException,
		RecordStoreException, 
		IOException,
		PartidaNoExisteException
		{
		
		RecordEnumeration re = rsPartidas.enumerateRecords(null,null,false);
		DataInputStream dis;
		int id;
		
		while (re.hasNextElement()) {
			id = re.nextRecordId(); 
			dis = new DataInputStream(new ByteArrayInputStream(rsPartidas.getRecord(id)));
			if (dis.readUTF().compareTo(nombre) == 0) {
				byte[] ba = new byte[dis.readInt()];
				dis.read(ba);
				sudoku.cargar(new ByteArrayInputStream(ba));
				return;
			}
		}
		
		throw new PartidaNoExisteException();
	}
	public void eliminarPartida(String nombre) throws
		RecordStoreNotOpenException, 
		InvalidRecordIDException,
		RecordStoreException, 
		IOException,
		PartidaNoExisteException
		{
		
		RecordEnumeration re = rsPartidas.enumerateRecords(null,null,false);
		DataInputStream dis;
		int id;
		
		while (re.hasNextElement()) {
			id = re.nextRecordId(); 
			dis = new DataInputStream(new ByteArrayInputStream(rsPartidas.getRecord(id)));
			if (dis.readUTF().compareTo(nombre) == 0) {
				rsPartidas.deleteRecord(id);
				return;
			}
		}
		
		throw new PartidaNoExisteException();
	}
}