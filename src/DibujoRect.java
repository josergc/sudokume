public class DibujoRect {
	public int ancho;
	public int alto;
	public int x;
	public int y;
	public int color;
	public DibujoRect(int x,int y,int ancho,int alto,int color) {
		this.ancho = ancho;
		this.alto = alto;
		this.x = x;
		this.y = y;
		this.color = color;
	}
	public DibujoRect(DibujoRect dr) {
		this.ancho = dr.ancho;
		this.alto = dr.alto;
		this.x = dr.x;
		this.y = dr.y;
		this.color = dr.color;
	}
}