/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase contiene algunos efectos sonoros para el juego.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/
import java.io.*;
import javax.microedition.media.*;
import javax.microedition.media.control.*;

public class EfectosSonoros {
	public final static byte tempo = 30; // Pone el tempo a 120 bpm
	public final static byte d = 8; // Nota octava
	public final static byte C4 = ToneControl.C4;
	public final static byte A4 = (byte)(C4 - 4);
	public final static byte B4 = (byte)(C4 - 2);
	public final static byte D4 = (byte)(C4 + 2);
	public final static byte E4 = (byte)(C4 + 4);
	public final static byte F4 = (byte)(C4 + 5);
	public final static byte G4 = (byte)(C4 + 7);
	public final static byte SILENCIO = ToneControl.SILENCE;
	
	public final static byte[][] secuencia = {
		{
			ToneControl.VERSION, 1,
			ToneControl.TEMPO, tempo,
			F4,d,B4,d
		}, {
			ToneControl.VERSION, 1,
			ToneControl.TEMPO, tempo,
			D4,d
		}, {
			ToneControl.VERSION, 1,
			ToneControl.TEMPO, tempo,
			B4,d,F4,d
		}
	};
	
	public static Player[] pSecuencia = new Player[secuencia.length];
	public static String mensajeErrorAlIniciar = null;
		
	static {
		try {
			for (int i = 0; i < pSecuencia.length; i++)
				pSecuencia[i] = iniciaSecuencia(secuencia[i]);
		} catch (IOException e) {
			mensajeErrorAlIniciar = e.toString();
		} catch (MediaException e) {
			mensajeErrorAlIniciar = e.toString();
		}
	}
	
	protected static Player iniciaSecuencia(byte[] secuencia) throws
		IOException,
		MediaException
		{
		Player p = Manager.createPlayer(Manager.TONE_DEVICE_LOCATOR);
		p.realize();
		ToneControl c = (ToneControl)p.getControl("ToneControl");
		c.setSequence(secuencia);
		return p;
	}
	
}
