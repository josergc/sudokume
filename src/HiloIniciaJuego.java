/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Este hilo inicia un juego. Esto est� porque a la hora de generar un 
	tablero, dependiendo de la dificultad, puede tardar m�s o menos en 
	generarlo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

public class HiloIniciaJuego implements Runnable {
	public void run() {
		SudokuME.sudoku.ponDificultad(
			SudokuME.configuracion.dificultadSudoku
			);
		SudokuME.sudoku.ponTiempoInicial(
			SudokuME.configuracion.sudokuConTiempo
			);
		SudokuME.sudoku.generaTablero();
		SudokuME.pantallaJuego = new PantallaJuego(
			SudokuME.configuracion.sudokuConAyuda,
			SudokuME.configuracion.sudokuConTiempo
			);
		SudokuME.gestionPantallas.ponPantalla(SudokuME.pantallaJuego);
	}
}