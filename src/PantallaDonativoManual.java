import javax.microedition.lcdui.*;

public class PantallaDonativoManual extends Form implements CommandListener, Pantalla {
	protected TextField mensaje;
	protected TextField telefono;
	
	protected final static Command cmdEnviar = new Command("Enviar",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);
	
	public PantallaDonativoManual() {
		super("Env�o del donativo manual");
		
		append(new StringItem("Atenci�n","Esta forma de env�o es para el caso en el que se a�adiera alg�n pa�s a la lista despu�s de la publicaci�n o que se hubiera cambiado el mensaje a enviar o el tel�fono a donde enviar el mensaje. M�s informaci�n en el apartado de \'donativos\' en www.josergc.tk"));
		append(mensaje = new TextField("Mensaje","EN SEPOMO",255,TextField.ANY));
		append(telefono = new TextField("Tel�fono","5522",255,TextField.PHONENUMBER));
		
		addCommand(cmdEnviar);
		addCommand(cmdVolver);
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		if (c == cmdEnviar) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaEnviarMensaje(mensaje.getString(),telefono.getString())
				);
		} else
			SudokuME.gestionPantallas.pantallaCerrada();
	}
	public void activarPantalla() {
	}
}
