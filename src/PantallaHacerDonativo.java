/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Esta pantalla muestra un texto explicando las distintas formas de hacer un 
	donativo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import javax.microedition.lcdui.*;

public class PantallaHacerDonativo extends Form implements CommandListener, Pantalla {
	protected final static Command cmdDonar = new Command("Hacer un micropago",Command.ITEM,1);
	protected final static Command cmdIntroducirCodigo = new Command("Introducir c�digo",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);

	public PantallaHacerDonativo() {
		super("Haz un donativo");
		
		append(new StringItem(
			"Haz un donativo",
			"Si te ha gustado esta aplicaci�n, puedes hacer un donativo para animarme a realizar m�s proyectos."
			));
		append(new StringItem(
			"�C�mo hacerlo?","Puedes realizar tu donativo de diversas maneras."
			));
		append(new StringItem(
			"Mediante un micropago",
			"La aplicaci�n enviar� un SMS seleccionando antes el pa�s donde est�s. Deber�as recibir un SMS con un c�digo que o bien puedes introducirlo en el apartado 'Introducir c�digo' en esta pantalla o en el apartado 'Donativos' en www.josergc.tk"
			));
		append(new StringItem(
			"A trav�s de PayPal",
			"Si dispones de una cuenta de PayPal, puedes hacer tu donativo a josergc@lycos.es"
			));
		append(new StringItem(
			"Otros",
			"Si quieres hacer cualquier otro tipo de donativo o mediante otra manera, ponte en contacto conmigo enviando un e-mail a josergc@lycos.es"
			));
		append(new StringItem(
			"Ante todo",
			"Darte las gracias por usar la aplicaci�n y molestarte en leer esto :-)"
			));
		append(new StringItem(
			"El autor",
			"Jos� Roberto Garc�a Chico"
			));
		
		addCommand(cmdDonar);
		addCommand(cmdIntroducirCodigo);
		addCommand(cmdVolver);
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		if (c == cmdDonar) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaHacerMicropago()
				);
		}else if (c == cmdIntroducirCodigo) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaIntroducirCodigoMicropago()
				);
		} else
			SudokuME.gestionPantallas.pantallaCerrada();
	}
	public void activarPantalla() {
	}
}