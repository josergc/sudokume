/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase lleva el control de una partida y su actual estado. Adem�s, 
	tambi�n proporciona mecanismos para guardar la partida y recuperarla
	despu�s desde un flujo de datos.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.io.*;

public class Sudoku {
	protected java.util.Random r = new java.util.Random(new java.util.Date().getTime());
	protected int dificultad;
	protected long tiempoInicial;
	protected long tiempoRestante;
	public int[][] tablero = new int[9][9];
	public boolean[][] casillasCubiertas = new boolean[9][9];
	public int[][] casillaSolucionada = new int[9][9];
	
	public Sudoku() {
		reiniciaTablero();
		ponDificultad(0);
	}
	public void ponDificultad(int nuevaDificultad) {
		dificultad = nuevaDificultad;
	}
	public void ponTiempoInicial(long tiempoInicial) {
		this.tiempoInicial = tiempoRestante = tiempoInicial;
	}
	public long devTiempoInicial(){
		return tiempoInicial;
	}
	public long devTiempoRestante() {
		return tiempoRestante;
	}
	public void decTiempoRestante(long d) {
		if (tiempoRestante > 0)
			tiempoRestante -= d;
	}
	public void reiniciaPartida() {
		for (int i = 0; i < tablero.length; i++)
			for (int j = 0; j < tablero[i].length; j++) 
				casillaSolucionada[i][j] = 0;
		tiempoRestante = tiempoInicial;
	}
	public void reiniciaTablero() {
		for (int i = 0; i < tablero.length; i++)
			for (int j = 0; j < tablero[i].length; j++) {
				tablero[i][j] = 0;
				casillasCubiertas[i][j] = false;
				casillaSolucionada[i][j] = 0;
			}
		tiempoRestante = tiempoInicial;
	}
	public boolean generaTablero() {
		reiniciaTablero();
		return ponNumero(0,0);
	}
	protected boolean ocultaCasillas(int bloqueX, int bloqueY, int faltanPorOcultar) {
		if (bloqueX == 0 && bloqueY == 3)
			return faltanPorOcultar == 0;
		int numeroCasillasAOcultarInicial = Math.abs(r.nextInt() % (2 + dificultad)) + 1;
		int numeroCasillasAOcultar = numeroCasillasAOcultarInicial;
		int[] x = new int[(2 + dificultad) + 1];
		int[] y = new int[(2 + dificultad) + 1];
		int i;
		int siguienteBloqueX;
		int siguienteBloqueY;
		
		if (bloqueX == 2) {
			siguienteBloqueX = 0;
			siguienteBloqueY = bloqueY + 1;
		} else {
			siguienteBloqueX = bloqueX + 1;
			siguienteBloqueY = bloqueY;
		}
		
		do {
			for (i = 0; i < numeroCasillasAOcultar; i++) {
				do {
					y[i] = (bloqueY * 3) + Math.abs(r.nextInt() % 3);
					x[i] = (bloqueX * 3) + Math.abs(r.nextInt() % 3);
				} while(casillasCubiertas[y[i]][x[i]]);
				casillasCubiertas[y[i]][x[i]] = true;
			}
			if (ocultaCasillas(
				siguienteBloqueX,
				siguienteBloqueY,
				faltanPorOcultar - numeroCasillasAOcultar
				))
				return true;
			for (i = 0; i < numeroCasillasAOcultar; i++)
				casillasCubiertas[y[i]][x[i]] = false;
			if (numeroCasillasAOcultar == (2 + dificultad) + 1)
				numeroCasillasAOcultar = 1;
			else
				numeroCasillasAOcultar++;
		} while(numeroCasillasAOcultar != numeroCasillasAOcultarInicial);
		
		return false;
	}
	protected boolean ponNumero(int fila, int columna) {
		if (fila == 9) {
			// Oculta casillas dependiendo del nivel de dificultad
			return ocultaCasillas(0,0,(2 + dificultad) * 9);
		} else {
			// Rellena el tablero
			int numeroInicial = Math.abs(r.nextInt() % 9) + 1;
			tablero[fila][columna] = numeroInicial;
			do {
				if (compruebaFila(fila,columna)) {
					if (compruebaColumna(fila,columna)) {
						if (compruebaBloque(fila,columna)) {
							int siguienteColumna = (columna + 1) % 9;
							int siguienteFila = siguienteColumna == 0 ? (fila + 1) : fila;
							if (ponNumero(siguienteFila,siguienteColumna)) {
								return true;
							} else
								tablero[fila][columna] = siguienteNumero(tablero[fila][columna]);
						} else
							tablero[fila][columna] = siguienteNumero(tablero[fila][columna]);
					} else
						tablero[fila][columna] = siguienteNumero(tablero[fila][columna]);
				} else
					tablero[fila][columna] = siguienteNumero(tablero[fila][columna]);
			} while(tablero[fila][columna] != numeroInicial);
			tablero[fila][columna] = 0;
			return false;
		}
	}
	protected boolean compruebaFila(int fila, int columna) {
		// Compruebo primero si el �ltimo n�mero puesto no est� repetido
		for (int i = columna - 1; i >= 0; i--)
			if (tablero[fila][columna] == tablero[fila][i])
				return false;
		return true;
	}
	protected boolean compruebaColumna(int fila, int columna) {
		// Compruebo primero si el �ltimo n�mero puesto no est� repetido
		for (int i = fila - 1; i >= 0; i--)
			if (tablero[fila][columna] == tablero[i][columna])
				return false;
		return true;
	}
	protected boolean compruebaBloque(int fila, int columna) {
		// Compruebo en el bloque no haya valor repetido
		int bloqueInicioX = (columna / 3) * 3;
		int bloqueInicioY = (fila / 3) * 3;
		int bloqueFinX = bloqueInicioX + 3 - 1;
		
		for (int y = fila; y >= bloqueInicioY; y--)
			for (int x = (y == fila) ? columna : bloqueFinX; x >= bloqueInicioX; x--) 
				if (x != columna && y != fila) 
					if (tablero[y][x] == tablero[fila][columna])
						return false;
		return true;
	}
	protected int siguienteNumero(int numero) {
		return numero == 9 ? 1 : (numero + 1);
	}
	public void cargar(InputStream is) throws
		EOFException,
		IOException
		{
		DataInputStream dis = new DataInputStream(is);
		dificultad = dis.readInt();
		tiempoInicial = dis.readLong();
		tiempoRestante = dis.readLong();
		for (int y = tablero.length - 1; y >= 0; y--) 
			for (int x = tablero[y].length - 1; x >= 0; x--) {
				tablero[y][x] = dis.readInt();
				casillasCubiertas[y][x] = dis.readBoolean();
				casillaSolucionada[y][x] = dis.readInt();
			}
	}
	public void guardar(OutputStream os) throws 
		IOException
		{
		DataOutputStream dos = new DataOutputStream(os);
		dos.writeInt(dificultad);
		dos.writeLong(tiempoInicial);
		dos.writeLong(tiempoRestante);
		for (int y = tablero.length - 1; y >= 0; y--)
			for (int x = tablero[y].length - 1; x >= 0; x--) {
				dos.writeInt(tablero[y][x]);
				dos.writeBoolean(casillasCubiertas[y][x]);
				dos.writeInt(casillaSolucionada[y][x]);
			}
	}
}