/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	En esta pantalla podemos introducir el c�digo recibido para hacer el 
	micropago efectivo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;

public class PantallaIntroducirCodigoMicropago extends Form implements CommandListener, Pantalla {
	protected TextField codigo = new TextField(
		"Introduce aqu� el c�digo",
		"",
		50,
		TextField.ANY
		);
	
	protected static final Command cmdAceptar = new Command("Aceptar",Command.OK,1);
	protected static final Command cmdCancelar = new Command("Cancelar",Command.CANCEL,1);

	public PantallaIntroducirCodigoMicropago() {
		super("Introducir c�digo");
		
		append(new StringItem(
			"Confirmar micropago",
			"En esta pantalla puedes hacer efectivo tu micropago introduciendo el c�digo del SMS recibido."
			));
		append(codigo);
		
		addCommand(cmdAceptar);
		addCommand(cmdCancelar);
		
		setCommandListener(this);
	}
	
	public void commandAction(Command c, Displayable d) {
		if (c == cmdAceptar) {
			SudokuME.gestionPantallas.ponPantalla(new PantallaEnviarCodigoMicropago(
				codigo.getString()
				));
		} else {
			SudokuME.gestionPantallas.pantallaCerrada();
		}
	}
	
	public void activarPantalla() {
	}
}
