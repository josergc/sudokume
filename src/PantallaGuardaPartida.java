import javax.microedition.lcdui.*;

public class PantallaGuardaPartida extends Form implements CommandListener, Pantalla {
	public static final int CMD_ACEPTAR = 0;
	public static final int CMD_CANCELAR = 1;
	protected int ultimoComando;
	protected Command cmdAceptar;
	protected Command cmdCancelar;
	protected TextField nombre;
	public PantallaGuardaPartida(String nombrePartida) {
		super("");
		
		append(nombre = new TextField("Nombre de la partida a guardar",nombrePartida,256,TextField.ANY));
		addCommand(cmdAceptar = new Command("Aceptar",Command.OK,1));
		addCommand(cmdCancelar = new Command("Cancelar",Command.CANCEL,1));
		setCommandListener(this);
		
		ultimoComando = CMD_CANCELAR;
	}
	public int devUltimoComando() {
		return ultimoComando;
	}
	public void commandAction(Command c, Displayable d) {
		if (c == cmdAceptar) {
			ultimoComando = CMD_ACEPTAR;
			try {
				SudokuME.nombrePartida = devNombre();
				SudokuME.gestionJuego.guardarPartida(SudokuME.nombrePartida,SudokuME.sudoku);
				SudokuME.gestionPantallas.pantallaCerrada();
			} catch(Exception e) {
				SudokuME.gestionPantallas.ponPantalla(new PantallaError(e.toString()));
			}
		} else {
			ultimoComando = CMD_CANCELAR;
			SudokuME.gestionPantallas.pantallaCerrada();
		} 
	}
	public void activarPantalla() {
	}
	public String devNombre() {
		return nombre.getString();
	}
}