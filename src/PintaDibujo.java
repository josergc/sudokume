import java.util.*;
import javax.microedition.lcdui.*;

public class PintaDibujo {
	public static void desde(Graphics g, int x, int y, DibujoRect[] buffer) {
		DibujoRect dr;
		for (int i = buffer.length - 1; i >= 0; i--) {
			g.setColor((dr = buffer[i]).color);
			g.fillRect(dr.x + x,dr.y + y,dr.ancho,dr.alto);
		}
	}
	public static void desde(Graphics g, int x, int y, DibujoRect[] buffer, int[] paleta) {
		DibujoRect dr;
		for (int i = buffer.length - 1; i >= 0; i--) {
			g.setColor(paleta[(dr = buffer[i]).color]);
			g.fillRect(dr.x + x,dr.y + y,dr.ancho,dr.alto);
		}
	}
	final static int precision = 8;
	final static int _1 = 1 << precision;
	public static void escala(Graphics g, Dibujo d, int nuevoAncho, int nuevoAlto) {
		if (d.tipo == Dibujo.TIPO_VECTOR) {
		} else if (d.tipo == Dibujo.TIPO_RGB) {
			int c;
			int x, x1, x11, tx1, y, y1, y11, Y, ty1;
			int xFinal = d.ancho << precision;
			int xFinal1 = nuevoAncho << precision;
			int yFinal = d.alto << precision;
			int yFinal1 = nuevoAlto << precision;
			int dx, dx1, dy, dy1, DY;
			if (nuevoAncho >= d.ancho) {
				dx = _1;
				dx1 = xFinal1 / d.ancho;
			} else {
				dx = xFinal / nuevoAncho;
				dx1 = _1;
			}
			if (nuevoAlto >= d.alto) {
				dy = _1;
				dy1 = yFinal1 / d.alto;
			} else {
				dy = yFinal / nuevoAlto;
				dy1 = _1;
			}
			DY = dy * d.ancho;
			for (
				y = 0, y1 = 0, Y = 0, y11 = dy1; 
				y < yFinal; 
				y += dy, y1 += dy1, Y += DY, y11 += dy1
				) {
				for (
					x = 0, x1 = 0, x11 = dx1; 
					x < xFinal; 
					x += dx, x1 += dx1, x11 += dx1
					) {
					c = (x + Y) >> precision;
					if (c < d.buffer.length)
					if (d.buffer[c] != -1) {
						g.setColor(d.buffer[c]);
						g.fillRect(
							tx1 = (x1 >> precision),
							ty1 = (y1 >> precision),
							(x11 >> precision) - tx1,
							(y11 >> precision) - ty1
							);
					}
				}
			}
		} else if (d.tipo == Dibujo.TIPO_RGBPALETA) {
			int c;
			int x, x1, x11, tx1, y, y1, y11, Y, ty1;
			int xFinal = d.ancho << precision;
			int xFinal1 = nuevoAncho << precision;
			int yFinal = d.alto << precision;
			int yFinal1 = nuevoAlto << precision;
			int dx, dx1, dy, dy1, DY;
			int[] paleta = d.paletaPropia != null ? d.paletaPropia : Dibujo.paleta;
			if (nuevoAncho >= d.ancho) {
				dx = _1;
				dx1 = xFinal1 / d.ancho;
			} else {
				dx = xFinal / nuevoAncho;
				dx1 = _1;
			}
			if (nuevoAlto >= d.alto) {
				dy = _1;
				dy1 = yFinal1 / d.alto;
			} else {
				dy = yFinal / nuevoAlto;
				dy1 = _1;
			}
			DY = dy * d.ancho;
			for (
				y = 0, y1 = 0, Y = 0, y11 = dy1; 
				y < yFinal; 
				y += dy, y1 += dy1, Y += DY, y11 += dy1
				) {
				for (
					x = 0, x1 = 0, x11 = dx1; 
					x < xFinal; 
					x += dx, x1 += dx1, x11 += dx1
					) {
					c = (x + Y) >> precision;
					if (c < d.bufferPaleta.length)
					if (d.bufferPaleta[c] != -1) {
						g.setColor(paleta[d.bufferPaleta[c]]);
						g.fillRect(
							tx1 = (x1 >> precision),
							ty1 = (y1 >> precision),
							(x11 >> precision) - tx1,
							(y11 >> precision) - ty1
							);
					}
				}
			}
		}
	}
}