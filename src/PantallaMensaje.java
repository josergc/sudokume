/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Muestra una pantalla con un mensaje dado.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaMensaje extends Form implements CommandListener, Pantalla {
	protected static Image imgMensaje = null;
	protected Command cmdVolver;
	public PantallaMensaje(String mensaje) {
		super("Mensaje");
		inicia("Mensaje",mensaje);
	}
	public PantallaMensaje(String titulo, String mensaje) {
		super("Mensaje");
		inicia(titulo,mensaje);
	}
	protected void inicia(String titulo, String mensaje) {
		if (imgMensaje == null)
			try {
				imgMensaje = Image.createImage("/mensaje.png");
			} catch (IOException e) {
			}
		append(new ImageItem("",imgMensaje,ImageItem.LAYOUT_CENTER,""));
		String[] cadenas = Utiles.divideCadena(mensaje,'\n');
		append(new StringItem(titulo,null));
		for (int i = 0; i < cadenas.length; i++)
			append(new StringItem(null,cadenas[i]));
		addCommand(cmdVolver = new Command("Volver",Command.ITEM,1));
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		SudokuME.gestionPantallas.pantallaCerrada();
	}
	public void activarPantalla() {
	}
}