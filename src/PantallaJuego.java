/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase es la pantalla del juego, muestra el tablero centrado y un 
	cursor parpadeante. Usando las teclas del cursor, puedes mover el 
	cursor a trav�s del tablero. El bot�n de acci�n/central es para entrar 
	en el men� de opciones y los botones de los n�meros es para introducir
	el n�mero que pensamos que es si la casilla est� vac�a.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.io.*;
import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.media.*;
import javax.microedition.media.control.*;

public class PantallaJuego extends Canvas implements CommandListener, Runnable, Pantalla {

	// Inicia las constantes de la pantalla
	protected static final int colorFondo = 0x000077;
	protected static final int colorAmarillo = 0xffff99;
	protected static final int colorAzul = 0x0000ff;
	protected static final int colorRojo = 0xff0000;
	protected static final int colorBlanco = 0xffffff;
	protected static final int colorCursor = 0x009900;
	protected static final int colorTiempo = 0x00eeff;
	protected static final int colorTiempoParpadea1 = 0xffffff;
	protected static final int colorTiempoParpadea2 = 0x000000;
	protected static final int colorTiempoPocoTiempo = 0xff0000;

	// Variables del juego
	protected boolean redibujaBufferTablero;
	protected Image bufferTablero;
	protected Graphics gBufferTablero;
	protected boolean redibujaBufferTableroJugando;
	protected Image bufferTableroJugando;
	protected Graphics gBufferTableroJugando;
	protected int xTablero;
	protected int yTablero;
	protected int anchoTablero;
	protected int altoTablero;
	protected int xCursor;
	protected int yCursor;
	protected int retrasoParpadeoCursor;
	protected boolean subeCursor;
	protected boolean bajaCursor;
	protected boolean izquierdaCursor;
	protected boolean derechaCursor;
	protected boolean pintaCursor;
	protected boolean ayudaActivada;
	protected int ultimoNumeroPuesto;
	protected boolean[] poner;
	protected boolean jugando;
	protected long tiempoLimite;
	protected long tiempoRestante;
	protected boolean noFinDelJuego;
	protected boolean pantallaNoActiva;
	
	// Comandos del men�	
	protected static final Command cmdAcciones = new Command("Acciones",Command.ITEM,1);
	protected static final Command cmdOpciones = new Command("Opciones",Command.ITEM,1);
	protected static final Command cmdAyuda = new Command("Ayuda",Command.ITEM,1);
	
	public static final int CMD_NADA = -1;
	public static final int CMD_ACCIONES = 0;
	public static final int CMD_OPCIONES = 1;
	public static final int CMD_AYUDA = 2;
	public static final int CMD_CARGAR = 3;
	public static final int CMD_GUARDAR = 4;
	protected int ultimoComando = CMD_NADA;
	
	public PantallaJuego(boolean conAyuda, long tiempo) {
		super();
		
		altoTablero = SudokuME.sudoku.tablero.length * SudokuME.tamCasilla;
		anchoTablero = SudokuME.sudoku.tablero[0].length * SudokuME.tamCasilla;
		yTablero = (Constantes.altoPantalla - (altoTablero + 1)) / 2;
		xTablero = (Constantes.anchoPantalla - (anchoTablero + 1)) / 2;
		bufferTablero = Image.createImage(anchoTablero + 1,altoTablero + 1);
		gBufferTablero = bufferTablero.getGraphics();
		bufferTableroJugando = Image.createImage(anchoTablero + 1,altoTablero + 1);
		gBufferTableroJugando = bufferTableroJugando.getGraphics();
		redibujaBufferTablero = true;
		redibujaBufferTableroJugando = true;
		yCursor = SudokuME.sudoku.tablero.length >> 1;
		xCursor = SudokuME.sudoku.tablero[0].length >> 1;
		retrasoParpadeoCursor = 0;
		subeCursor = false;
		bajaCursor = false;
		izquierdaCursor = false;
		derechaCursor = false;
		pintaCursor = false;
		ayudaActivada = conAyuda;
		noFinDelJuego = true;
		tiempoRestante = tiempoLimite = tiempo;
		ultimoNumeroPuesto = -1;
		poner = new boolean[9];
		jugando = true;
		pantallaNoActiva = true;
		
		addCommand(cmdAcciones);
		addCommand(cmdOpciones);
		addCommand(cmdAyuda);
		setCommandListener(this);
	}
	
	public void paint(Graphics g) {
		g.setColor(colorFondo);
		g.fillRect(0,0,Constantes.anchoPantalla,Constantes.altoPantalla);
		
		// Redibuja el buffer del tablero de juego que contiene las casillas 
		// con los n�meros de apoyo, espacios en blanco que son los huecos a 
		// rellenar y las casillas donde el jugador ha puesto alg�n n�mero.
		if (redibujaBufferTablero) {
			for (
				int y = 0, y1 = 0, bloque = 0; 
				y < SudokuME.sudoku.tablero.length; 
				y++, y1 += SudokuME.tamCasilla
				) 
				for (
					int x = 0, x1 = 0; 
					x < SudokuME.sudoku.tablero[0].length; 
					x++, x1 += SudokuME.tamCasilla
					) {
					bloque = (y / 3) * 3 + (x / 3);
					if (SudokuME.sudoku.casillaSolucionada[y][x] != 0) {
						gBufferTablero.drawImage(
							SudokuME.numerosAzules[SudokuME.sudoku.tablero[y][x] - 1],
							x1, 
							y1,
							Constantes.TOPLEFT
							);
					} else if (SudokuME.sudoku.casillasCubiertas[y][x]) {
						gBufferTablero.setColor((bloque & 1) == 0 ? colorAmarillo : colorBlanco);
						gBufferTablero.fillRect(x1,y1,SudokuME.tamCasilla,SudokuME.tamCasilla);
					} else
						gBufferTablero.drawImage(
							(bloque & 1) == 0 ? SudokuME.numerosAmarillos[SudokuME.sudoku.tablero[y][x] - 1] : SudokuME.numerosBlancos[SudokuME.sudoku.tablero[y][x] - 1],
							x1, 
							y1,
							Constantes.TOPLEFT
							);
				}
			redibujaBufferTablero = false;
		}
		
		// En este buffer se copia el tablero de juego y se pinta las casillas 
		// donde el jugador ha puesto n�meros de color rojo si el n�mero es 
		// err�neo y est� la ayuda activada o de color azul si no es err�neo.
		// Adem�s, pinta l�neas delimitadoras de columnas y filas.
		if (redibujaBufferTableroJugando) {
			gBufferTableroJugando.drawImage(bufferTablero,0,0,Constantes.TOPLEFT);
			for (
				int y = 0, y1 = 0, bloque = 0; 
				y < SudokuME.sudoku.tablero.length; 
				y++, y1 += SudokuME.tamCasilla
				) 
				for (
					int x = 0, x1 = 0; 
					x < SudokuME.sudoku.tablero[0].length; 
					x++, x1 += SudokuME.tamCasilla
					) {
					bloque = (y / 3) * 3 + (x / 3);
					if (SudokuME.sudoku.casillaSolucionada[y][x] != 0) 
						gBufferTableroJugando.drawImage(
							SudokuME.sudoku.casillaSolucionada[y][x] != SudokuME.sudoku.tablero[y][x] && ayudaActivada ? SudokuME.numerosRojos[(jugando ? SudokuME.sudoku.casillaSolucionada[y][x] : SudokuME.sudoku.tablero[y][x]) - 1] : SudokuME.numerosAzules[SudokuME.sudoku.casillaSolucionada[y][x] - 1],
							x1, 
							y1,
							Constantes.TOPLEFT
							);
				}
			gBufferTableroJugando.setColor(0);
			for (int y = 0, y1 = 0; y <= SudokuME.sudoku.tablero.length; y++, y1 += SudokuME.tamCasilla)
				gBufferTableroJugando.drawLine(0,y1,anchoTablero,y1);
			for (int x = 0, x1 = 0; x <= SudokuME.sudoku.tablero[0].length; x++, x1 += SudokuME.tamCasilla)
				gBufferTableroJugando.drawLine(x1,0,x1,altoTablero);
			redibujaBufferTableroJugando = false;
		}
		
		// Pone el �ltimo buffer en pantalla
		g.drawImage(bufferTableroJugando,xTablero,yTablero,Constantes.TOPLEFT);
		
		// Si se est� jugando, pinta el cursor
		if (jugando) {
			g.setColor(colorCursor);
			if (pintaCursor)
				g.fillRect(
					xTablero + xCursor * SudokuME.tamCasilla,
					yTablero + yCursor * SudokuME.tamCasilla,
					SudokuME.tamCasilla,
					SudokuME.tamCasilla
					);
			else
				g.drawRect(
					xTablero + xCursor * SudokuME.tamCasilla,
					yTablero + yCursor * SudokuME.tamCasilla,
					SudokuME.tamCasilla,
					SudokuME.tamCasilla
					);
		}
		
		// Pinta el tiempo restante
		if (SudokuME.sudoku.devTiempoInicial() > 0) {
			long t = SudokuME.sudoku.devTiempoRestante() / (1000);
			long m = t / 60;
			long s = t % 60;
			String tiempoRestante = s < 10 ? (m + ":0" + s) : (m + ":" + s);
			// Si quedan 10 o menos segundos, pinta el tiempo de otro color. Si 
			// se ha acabado el tiempo, lo pinta de manera que alterne dos 
			// colores distintos
			g.setColor((m == 0) ? (s == 0 ? (pintaCursor ? colorTiempoParpadea2 : colorTiempoParpadea1) : (s <= 10 ? colorTiempoPocoTiempo : colorTiempo)) : colorTiempo);
			if (Constantes.orientacionVertical)
				g.drawString(tiempoRestante,Constantes.anchoPantalla >> 1,yTablero >> 1,Constantes.TOPHCENTER);
			else
				Utiles.pintaCadenaVerticalCentrada(g,tiempoRestante,xTablero >> 1,Constantes.altoPantalla >> 1);
		}
		
		g.setColor(0xffffff);
		g.drawString(pantallaNoActiva ? "NoActiva" : "Activa",0,0,Constantes.TOPLEFT);
		g.drawString("ultimoComando=" + ultimoComando,0,g.getFont().getHeight(),Constantes.TOPLEFT);
		g.drawString(noFinDelJuego ? "noFinDelJuego" : "inDelJuego",0,g.getFont().getHeight()*2,Constantes.TOPLEFT);
	}
	
	public void run() {
		// Si se est� jugando, hace que parpadee el cursor y si se 
		// recibe alguna orden de mover el cursor, lo mueve. O si se
		// recibe alguna orden de poner alg�n n�mero, se pone el n�mero
		// y se indica que el segundo buffer debe de ser redibujado.
		if (jugando) {
			if ((retrasoParpadeoCursor++ & 3) == 0)
				pintaCursor = !pintaCursor;
		
			if (subeCursor) {
				if (yCursor > 0)
					yCursor--;
				subeCursor = false;
			}
			if (bajaCursor) {
				if (yCursor < SudokuME.sudoku.tablero.length - 1)
					yCursor++;
				bajaCursor = false;
			}
			if (izquierdaCursor) {
				if (xCursor > 0)
					xCursor--;
				izquierdaCursor = false;
			}
			if (derechaCursor) {
				if (xCursor < SudokuME.sudoku.tablero[0].length - 1)
					xCursor++;
				derechaCursor = false;
			}
			if (ultimoNumeroPuesto != -1) {
				SudokuME.sudoku.casillaSolucionada[yCursor][xCursor] = ultimoNumeroPuesto;
				redibujaBufferTableroJugando = true;
				ultimoNumeroPuesto = -1;
			}
			SudokuME.sudoku.decTiempoRestante(50);
		} else {
			// Si no se est� jugando y no es el fin del juego, 
			// activa la ayuda para ver la soluci�n del panel, 
			// redibuja el segundo buffer y deja puesto el modo de 
			// final del juego donde no se puede hacer otra cosa 
			// que la de salir de la partida o cargar alguna 
			// partida salvada.
			if (noFinDelJuego) {
				ayudaActivada = true;
				redibujaBufferTableroJugando = true;
				noFinDelJuego = false;
			}
		}
		
		repaint();
		
		try { Thread.sleep(50); } catch (InterruptedException e) { return; }
		
		// Si la pantalla actual es esta, se comprueba si se ha dado 
		// alguna orden para salir de ella. Si no, controla que se siga
		// ejecutando esta funci�n despu�s de que se repinte la 
		// pantalla y procura que no se corte el sonido.
		//if (SudokuME.gestionPantallas.devPantallaActual() == this) {
		if (Constantes.d.getCurrent() == this) {
			if (pantallaNoActiva) {
				SudokuME.gestionPantallas.pantallaCerrada();
			} else {
				Constantes.d.callSerially(this);
				SudokuME.controlSonido(SudokuME.melodiaJuego);
			}
		}
	}
	
	protected void keyPressed(int keyCode) {
		if (pantallaNoActiva)
			return;
		if (jugando) {
			if (keyCode == KEY_NUM0) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 0;
					SudokuME.tocarSecuencia(2);
				}
			} else if (keyCode == KEY_NUM1) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 1;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM2) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 2;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM3) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 3;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM4) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 4;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM5) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 5;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM6) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 6;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM7) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 7;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM8) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 8;
					SudokuME.tocarSecuencia(0);
				}
			} else if (keyCode == KEY_NUM9) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) {
					ultimoNumeroPuesto = 9;
					SudokuME.tocarSecuencia(0);
				}
			} else {
				int a = getGameAction(keyCode);
				if (a == Constantes.botonArriba) {
					SudokuME.tocarSecuencia(1);
					subeCursor = true;
				} else if (a == Constantes.botonAbajo) {
					SudokuME.tocarSecuencia(1);
					bajaCursor = true;
				} else if (a == Constantes.botonIzquierda) {
					SudokuME.tocarSecuencia(1);
					izquierdaCursor = true;
				} else if (a == Constantes.botonDerecha) {
					SudokuME.tocarSecuencia(1);
					derechaCursor = true;
				} else if (a == Constantes.botonDisparo) {
					ponerPantallaOpciones();
				}
			}
		} else {
			int a = getGameAction(keyCode);
			if (a == Constantes.botonDisparo) {
				ponerPantallaOpciones();
			}
		}
	}
	
	// Al activar esta pantalla en el m�vil, si la �ltima pantalla que se 
	// ha visitado ha sido la de gesti�n de partidas, comprueba si la 
	// �ltima opci�n que ha elegido el usuario es la de cargar una partida 
	// para reiniciar las variables como si de una partida nueva se 
	// tratara.
	// Si la �ltima pantalla era la de opciones, pone o quita n�mero, 
	// ejecuta la comprobaci�n del juego y, por lo tanto, lo finaliza, 
	// guarda la partida actual, reinicia el tablero o sale de la pantalla.
	public void activarPantalla() {
		pantallaNoActiva = false;
		if (ultimoComando == CMD_OPCIONES) {
			switch (noFinDelJuego ? SudokuME.pantallaJuegoOpciones.devUltimoComando() : SudokuME.pantallaJuegoOpcionesMinimas.devUltimoComando()) {
				case PantallaJuegoOpciones.CMD_COMPROBAR_SUDOKU:
					jugando = false;
					ayudaActivada = true;
					noFinDelJuego = false;
				break;
				case PantallaJuegoOpciones.CMD_GUARDAR_SUDOKU:
					pantallaNoActiva = false;
					ultimoComando = CMD_GUARDAR;
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaGuardaPartida(SudokuME.nombrePartida)
						);
				break;
				case PantallaJuegoOpciones.CMD_CARGAR_SUDOKU:
					pantallaNoActiva = false;
					ultimoComando = CMD_CARGAR;
					SudokuME.gestionPantallas.ponPantalla(
						SudokuME.pantallaPartidasTodasLasOpciones
						);
				break;
				case PantallaJuegoOpciones.CMD_REINICIA_SUDOKU:
					SudokuME.sudoku.reiniciaPartida();
					jugando = true;
					ayudaActivada = SudokuME.configuracion.sudokuConAyuda;
					noFinDelJuego = true;
				break;
				case PantallaJuegoOpciones.CMD_SALIR_SUDOKU:
					pantallaNoActiva = true;
					SudokuME.gestionPantallas.pantallaCerrada();
				break;
			}
		} else if (ultimoComando == CMD_CARGAR) {
			switch(SudokuME.pantallaPartidasTodasLasOpciones.devUltimoComando()) {
				case PantallaPartidas.CMD_CARGAR:
					jugando = true;
					ayudaActivada = SudokuME.configuracion.sudokuConAyuda;
					noFinDelJuego = true;
				break;
			}
		} else if (ultimoComando == CMD_ACCIONES) {
			if (SudokuME.pantallaAcciones.devUltimoComando() != PantallaAcciones.CMD_VOLVER) {
				if (SudokuME.sudoku.casillasCubiertas[yCursor][xCursor]) 
					ultimoNumeroPuesto = SudokuME.pantallaAcciones.devUltimoComando();
			}
		}
		redibujaBufferTablero = true;
		redibujaBufferTableroJugando = true;
	}
	
	public void juegoCargado() {
		jugando = true;
		ayudaActivada = SudokuME.configuracion.sudokuConAyuda;
		noFinDelJuego = true;
	}
	
	protected void ponerPantallaAcciones() {
		ultimoComando = CMD_ACCIONES;
		pantallaNoActiva = false;
		SudokuME.gestionPantallas.ponPantalla(SudokuME.pantallaAcciones);
	}
	
	protected void ponerPantallaOpciones() {
		ultimoComando = CMD_OPCIONES;
		pantallaNoActiva = false;
		if (noFinDelJuego)
			SudokuME.gestionPantallas.ponPantalla(SudokuME.pantallaJuegoOpciones);
		else
			SudokuME.gestionPantallas.ponPantalla(SudokuME.pantallaJuegoOpcionesMinimas);
	}
	
	protected void ponerPantallaAyuda() {
		ultimoComando = CMD_AYUDA;
		pantallaNoActiva = false;
		SudokuME.gestionPantallas.ponPantalla(new PantallaMensaje(
			"Ayuda",
			"Usa el cursor para moverte por la pantalla y las teclas num�ricas para poner los n�meros.\nLos n�meros puestos aparecen en color azul o en rojo si tienes la ayuda activada y el n�mero no es correcto.\nEl 0 borra el n�mero puesto.\nTambi�n puedes poner y quitar n�meros desde 'Acciones'.\nEn el men� de 'Opciones' podr�s cargar, guardar, reiniciar, comprobar el tablero o salir al men� principal del juego.\nPuedes entrar en 'Opciones' pulsando el bot�n de acci�n (bot�n central del cursor en algunos m�viles) o desde el men� de la pantalla de juego."
			));
	}
	
	public void commandAction(Command c, Displayable d) {
		if (pantallaNoActiva)
			return;
		if (c == cmdAcciones) {
			ponerPantallaAcciones();
		} else if (c == cmdOpciones) {
			ponerPantallaOpciones();
		} else if (c == cmdAyuda) {
			ponerPantallaAyuda();
		}
	}
}