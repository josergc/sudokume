/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase es la principal que contiene la mayor�a de las variables comunes 
	para toda la aplicaci�n. Adem�s, tambi�n proporciona servicios para el 
	control de las canciones del juego y para tocar las secuencias mel�dicas.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.io.*;
import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.media.*;
import javax.microedition.media.control.*;
import javax.microedition.midlet.*;
import javax.microedition.rms.*;

public class SudokuME extends MIDlet {
	public static int tamCasilla;
	public static Image[] numerosAmarillos;
	public static Image[] numerosAzules;
	public static Image[] numerosRojos;
	public static Image[] numerosBlancos;
	public static String nombrePartida;
	public static Sudoku sudoku;
	public static SudokuME sudokuME = null;
	public static GestionJuego gestionJuego;
	public static GestionPantallas gestionPantallas;
	public static Configuracion configuracion;
	public static PantallaPartidas pantallaPartidasTodasLasOpciones;
	public static PantallaPartidas pantallaPartidasSinOpcionNuevo;
	public static PantallaJuego pantallaJuego;
	public static PantallaJuegoOpciones pantallaJuegoOpcionesMinimas;
	public static PantallaJuegoOpciones pantallaJuegoOpciones;
	public static PantallaConfiguracion pantallaConfiguracion;
	public static PantallaPresentacion pantallaPresentacion;
	public static PantallaAcciones pantallaAcciones;
	public static String[] nombreMelodiaMenu = {
		"/M_Ashanti-Happy.mid"
		};
	public static Player[] melodiaMenu = new Player[nombreMelodiaMenu.length];
	public static String[] nombreMelodiaJuego = {
		/*"/Bach - Aria in G.mid",
		"/Canon.mid",*/
		"/Chopin-Preludio.mid"
		};
	public static Player[] melodiaJuego = new Player[nombreMelodiaJuego.length];
	public static int melodiaIndice = 0;
	public static Player[] melodia = null;
	
	public static void controlSonido(boolean activar) {
		try {
			if (melodia != null) {
				if (activar) {
					((VolumeControl)melodia[melodiaIndice].getControl("VolumeControl")).setLevel(configuracion.volumenMusica);
					melodia[melodiaIndice].start();
				} else 
					melodia[melodiaIndice].stop();
			}
		} catch(Exception e) {
			gestionPantallas.ponPantalla(
				new PantallaError("controlSonido",e.toString())
				);
		}
	}
	
	public static void controlSonido(Player[] melodiaATocar) {
		try {
			if (melodiaATocar == null) {
				if (melodia != null) 
					melodia[melodiaIndice].stop();
				return;
			}
			if (melodia != melodiaATocar) {
				if (melodia != null) {
					melodia[melodiaIndice].stop();
				}
				melodia = melodiaATocar;
				melodiaIndice = Math.abs(Constantes.r.nextInt()) % melodia.length;
			}
			if (melodia[melodiaIndice].getState() == Player.PREFETCHED)
				melodiaIndice = (melodiaIndice + 1) % melodia.length;
			if (configuracion.activarMusica) {
				((VolumeControl)melodia[melodiaIndice].getControl("VolumeControl")).setLevel(configuracion.volumenMusica);
				melodia[melodiaIndice].start();
			}
		} catch(Exception e) {
			gestionPantallas.ponPantalla(
				new PantallaError("controlSonido",e.toString())
				);
		}
	}

	public static void tocarSecuencia(int idSecuencia) {
		if (configuracion.activarSonidos)
			try {
				EfectosSonoros.pSecuencia[idSecuencia].stop();
				((VolumeControl)EfectosSonoros.pSecuencia[idSecuencia].getControl("VolumeControl")).setLevel(configuracion.volumenSonidos);
				EfectosSonoros.pSecuencia[idSecuencia].start();
			} catch(Exception e) {
				gestionPantallas.ponPantalla(
					new PantallaError("controlSonido",e.toString())
					);
			}
	}
	
	protected void startApp() throws 
		MIDletStateChangeException
		{
		if (sudokuME == null) {
			sudokuME = this;
			new Constantes(this);
		
			gestionPantallas = new GestionPantallas();
			gestionPantallas.ponPantalla(pantallaPresentacion = new PantallaPresentacion(),false);
			gestionPantallas.ponPantalla(new PantallaIniciaAplicacion(this));
		} else {
			controlSonido(configuracion.activarMusica);
			gestionPantallas.reactivaUltimaPantalla();
		}
	}
	
	protected void pauseApp() {
		controlSonido(false);
	}
	
	protected void destroyApp(boolean unconditional) throws 
		MIDletStateChangeException
		{
		try {
			gestionJuego.guardarConfiguracion();
		} catch (Exception e) {
		}
	}
	
	public void finalizar() throws
		MIDletStateChangeException
		{
		controlSonido(null);
		destroyApp(false);
		notifyDestroyed();
	}	
}