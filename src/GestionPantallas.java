/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Con objetos de esta clase ponemos las distintas pantallas de la aplicaci�n
	como activas y manteniendo un hist�rico de pantallas anteriores que todav�a
	est�n activas.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.util.Stack;
import java.util.EmptyStackException;
import javax.microedition.lcdui.Displayable;

public class GestionPantallas {
	protected Pantalla ultimaPantallaCerrada;
	protected Pantalla pantallaActual;
	protected Stack pantallas;
	
	public GestionPantallas() {
		ultimaPantallaCerrada = null;
		pantallaActual = null;
		pantallas = new Stack();
	}
	
	public void reactivaUltimaPantalla() {
		pantallaActual.activarPantalla();
		Constantes.d.setCurrent((Displayable)pantallaActual);
		if (pantallaActual instanceof Runnable)
			new Thread((Runnable)pantallaActual).start();
	}
	
	public void ponPantalla(Object o) {
		ponPantalla(o,true);
	}
	
	public void ponPantalla(Object o, boolean mostrar) {
		pantallas.push(pantallaActual);
		pantallaActual = (Pantalla)o;
		if (mostrar) {
			pantallaActual.activarPantalla();
			if (pantallaActual == (Pantalla)o) {
				Constantes.d.setCurrent((Displayable)o);
				if (o instanceof Runnable)
					new Thread((Runnable)o).start();
			}
		}
	}
	
	public void pantallaCerrada() {
		try {
			pantallas.peek();
		} catch(EmptyStackException e) {
			try {
				SudokuME.sudokuME.finalizar();
			} catch (Exception excepcion) {
				ponPantalla(new PantallaError("peek",e.toString()));
			}
			return;
		}
		Object o = pantallas.pop();
		if (o == null) 
			try {
				SudokuME.sudokuME.finalizar();
			} catch (Exception e) {
				ponPantalla(new PantallaError("pop",e.toString()));
			}
		else {
			ultimaPantallaCerrada = pantallaActual;
			pantallaActual = (Pantalla)o;
			((Pantalla)o).activarPantalla();
			if (pantallaActual == (Pantalla)o) {
				Constantes.d.setCurrent((Displayable)o);
				if (o instanceof Runnable) 
					Constantes.d.callSerially((Runnable)o);
			}
		}
	}
	
	public Pantalla devPantallaActual() {
		return pantallaActual;
	}
	
	public Pantalla devUltimaPantallaCerrada() {
		return ultimaPantallaCerrada;
	}
	
	public int pantallasApiladas() {
		return pantallas.size();
	}

}