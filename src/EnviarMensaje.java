import java.io.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.wireless.messaging.*;

public class EnviarMensaje extends Form implements Runnable, Pantalla {
	protected String mensajeAEnviar;
	protected String telefono;
	public EnviarMensaje(String mensajeAEnviar, String telefono) {
		super("Enviando...");
		
		this.mensajeAEnviar = mensajeAEnviar;
		this.telefono = telefono;
		
		append(new StringItem("Enviando mensaje","Espere por favor..."));
	}
	public void run() {
		// Env�a el mensaje
		synchronized (this) {
		try {
			MessageConnection mc = (MessageConnection)Connector.open("sms://" + telefono);
			TextMessage mensaje = (TextMessage)mc.newMessage(MessageConnection.TEXT_MESSAGE);
			mensaje.setPayloadText(mensajeAEnviar);
			mc.send(mensaje);
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaMensaje("Mensaje enviado","Ahora deber�a de recibir un SMS con el c�digo e ir al apartado 'donativos' en www.josergc.tk para hacer efectiva tu donaci�n.")
				);
			SudokuME.gestionPantallas.pantallaCerrada();
		} catch (IllegalArgumentException e) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaError("No se ha podido enviar el mensaje (IllegalArgumentException)",e.toString())
				);
			SudokuME.gestionPantallas.pantallaCerrada();
		} catch (IOException e) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaError("No se ha podido enviar el mensaje (IOException)",e.toString())
				);
			SudokuME.gestionPantallas.pantallaCerrada();
		} catch (Exception e) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaError("No se ha podido enviar el mensaje (Exception)",e.toString())
				);
			SudokuME.gestionPantallas.pantallaCerrada();
		}
		}
	}
	public void activarPantalla() {
	}
}