/**
	Nombre del proyecto:
	Comunes

	Descripci�n de la clase:
	Esta pantalla muestra una lista con los pa�ses desde donde se pueden hacer
	micropagos con Sepomo.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 05/11/2006
*/

import javax.microedition.lcdui.*;

public class PantallaHacerMicropago extends Lista implements CommandListener, Pantalla {
	protected final static Command cmdHacerMicropago = new Command("Hacer micropago",Command.ITEM,1);
	protected final static Command cmdVolver = new Command("Volver",Command.ITEM,1);

	public PantallaHacerMicropago() {
		super("Selecciona pa�s",IMPLICIT);
		
		for (int i = 0; i < PaisDonativo.paises.length; i++)
			append(PaisDonativo.paises[i].pais,null);
		append("--== Env�o manual ==--",null);
		append("--== Otros ==--",null);
		
		addCommand(cmdHacerMicropago);
		addCommand(cmdVolver);
		setCommandListener(this);
	}
	public void commandAction(Command c, Displayable d) {
		if (c == cmdHacerMicropago || c == SELECT_COMMAND) {
			if (getSelectedIndex() != -1) {
				if (getSelectedIndex() == PaisDonativo.paises.length) {
					// Env�o manual
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaDonativoManual()
						);
				} else if (getSelectedIndex() == PaisDonativo.paises.length + 1) {
					// Otros
					SudokuME.gestionPantallas.ponPantalla(new PantallaMensaje(
						"Si quieres enviar un donativo y tu pa�s no sale en la lista, es posible que quieras ver si se ha incorporado despu�s de la publicaci�n de esta aplicaci�n entrando en www.josergc.tk y en el apartado de \'Donativos\' o en www.sepomo.com"
						));
				} else {
					// Env�a el mensaje al pa�s elegido
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaEnviarMensaje(
							PaisDonativo.paises[getSelectedIndex()].cadenaAEnviar,
							PaisDonativo.paises[getSelectedIndex()].telefono
							)
						);
				}
			}
		} else if (c == cmdVolver) {
			SudokuME.gestionPantallas.pantallaCerrada();
		}
	}
	public void activarPantalla() {
	}
}