/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase muestra una pantalla que pregunta al jugador si desea 
	activar o no los sonidos de la aplicaci�n. Mientras, en segundo plano,
	va cargando e iniciando las im�genes y la m�sica del juego.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import javax.microedition.lcdui.*;
import javax.microedition.media.*;
import javax.microedition.media.control.*;

public class PantallaIniciaAplicacion extends Form implements Pantalla, Runnable, CommandListener {
	protected boolean haDecidido;
	protected boolean cargando;
	protected boolean activarSonidos;
	protected SudokuME sudokuME;
	protected Command cmdSi = new Command("S�",Command.OK,1);
	protected Command cmdNo = new Command("No",Command.CANCEL,1);

	public PantallaIniciaAplicacion(SudokuME sudokuME) {
		super("SudokuME");
		this.sudokuME = sudokuME;
		
		cargando = true;
		haDecidido = false;
		activarSonidos = false;
		append(new StringItem("Sudoku Microedition",null));
		append(new StringItem(null,"por Jos� Roberto Garc�a Chico\nwww.josergc.tk\njosergc@lycos.es"));
		append(new StringItem("�Activar sonidos?",null));
		addCommand(cmdSi);
		addCommand(cmdNo);
		setCommandListener(this);
	}
	
	/**
		Carga los ficheros MIDI de la lista especificada
	*/
	protected Player[] iniciaMidis(String[] nombreMelodia) {
		Player[] melodia = new Player[nombreMelodia.length];
		for (int i = 0; i < melodia.length; i++) 
			try {
				melodia[i] = Manager.createPlayer(
					getClass().getResourceAsStream(nombreMelodia[i]),
					"audio/midi"
					);
				melodia[i].prefetch();
			} catch (Exception e) {
				melodia[i] = null;
			}
		return melodia;
	}
	
	/**
		Pinta cada dibujo en cada imagen
	*/
	protected void generaImagenes(Image[] i, Dibujo[] d) {
		for (int j = 0; j < i.length; j++) 
			PintaDibujo.escala(
				(i[j] = Image.createImage(SudokuME.tamCasilla,SudokuME.tamCasilla)).getGraphics(),
				d[j],
				SudokuME.tamCasilla,
				SudokuME.tamCasilla
				);
	}
	
	/**
		Inicia la carga de datos
	*/
	public void run() {
		if (!cargando) return;

		// Inicia la configuraci�n por defecto
		SudokuME.configuracion = new Configuracion();
		
		// Configura el tama�o de la casilla que tendr� el tablero
		SudokuME.tamCasilla = (Constantes.minTamPantalla - 10) / 9;
		
		// Inicia las melod�as del juego
		SudokuME.melodiaMenu = iniciaMidis(SudokuME.nombreMelodiaMenu);
		SudokuME.melodiaJuego = iniciaMidis(SudokuME.nombreMelodiaJuego);
		
		// Genera las im�genes est�ticas
		generaImagenes(
			SudokuME.numerosAmarillos = new Image[9],
			new Dibujo[] {
				new Amarillo1(), new Amarillo2(), new Amarillo3(), new Amarillo4(), 
				new Amarillo5(), new Amarillo6(), new Amarillo7(), new Amarillo8(), 
				new Amarillo9()
			}
			);
		generaImagenes(
			SudokuME.numerosAzules = new Image[9],
			new Dibujo[] {
				new Azul1(), new Azul2(), new Azul3(), new Azul4(), 
				new Azul5(), new Azul6(), new Azul7(), new Azul8(), 
				new Azul9()
			}
			);
		generaImagenes(
			SudokuME.numerosRojos = new Image[9],
			new Dibujo[] {
				new Rojo1(), new Rojo2(), new Rojo3(), new Rojo4(), 
				new Rojo5(), new Rojo6(), new Rojo7(), new Rojo8(), 
				new Rojo9()
			}
			);
		generaImagenes(
			SudokuME.numerosBlancos = new Image[9],
			new Dibujo[] {
				new Blanco1(), new Blanco2(), new Blanco3(), new Blanco4(), 
				new Blanco5(), new Blanco6(), new Blanco7(), new Blanco8(), 
				new Blanco9()
			}
			);
		
		// Carga la configuraci�n
		try {
			SudokuME.gestionJuego = new GestionJuego();
			SudokuME.gestionJuego.cargarConfiguracion();
		} catch (Exception e) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaError("Error al cargar la configuraci�n",e.toString())
				);
		}
		
		// Tenemos dos pantallas iguales porque algunos dispositivos como el 
		// Nokia N-Gage no tiene implementada la opci�n de quitar elementos del 
		// men� desplegable
		SudokuME.pantallaPartidasTodasLasOpciones = new PantallaPartidas(true);
		SudokuME.pantallaPartidasSinOpcionNuevo = new PantallaPartidas(false);
		SudokuME.pantallaJuegoOpciones = new PantallaJuegoOpciones(true);
		SudokuME.pantallaJuegoOpcionesMinimas = new PantallaJuegoOpciones(false);
		SudokuME.pantallaConfiguracion = new PantallaConfiguracion();
		SudokuME.pantallaAcciones = new PantallaAcciones();
		
		SudokuME.nombrePartida = new String();
		SudokuME.sudoku = new Sudoku();

		cargando = false;
	
		// Esperamos hasta que el usuario haya decidido si activar o no los 
		// sonidos de la aplicaci�n
		while (!haDecidido) {
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
			}
		}
		SudokuME.configuracion.activarMusica = 
			SudokuME.configuracion.activarSonidos = 
			activarSonidos;
		SudokuME.gestionPantallas.pantallaCerrada();
	}
	public void activarPantalla() {
	}
	public void commandAction(Command c, Displayable d) {
		if (!haDecidido) {
			delete(size() - 1);
			append(new StringItem("Cargando...",null));
			if (c == cmdSi) {
				activarSonidos = true;
			} else if (c == cmdNo) {
				activarSonidos = false;
			}
			haDecidido = true;
		}
	}
}