/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase gestiona la configuraci�n global de la aplicaci�n como es si la 
	m�sica y los efectos sonoros est�n activados y con qu� volumen. Adem�s, 
	tambi�n lleva el nivel de dificultad, si la partida debe de tener la ayuda 
	activada y si es una partida con tiempo. Tambi�n proporciona mecanismos 
	para guardar y recuperar el estado desde un flujo de datos.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.io.*;

public class Configuracion {
	public boolean activarMusica = true;
	public int volumenMusica = 16 * 3;
	public boolean activarSonidos = true;
	public int volumenSonidos = 16 * 3;
	public int dificultadSudoku = 0;
	public boolean sudokuConAyuda = true;
	public long sudokuConTiempo = 0;
	
	public void guardar(OutputStream os) throws
		IOException
		{
		DataOutputStream dos = new DataOutputStream(os);
		
		dos.writeBoolean(activarMusica);
		dos.writeInt(volumenMusica);
		dos.writeBoolean(activarSonidos);
		dos.writeInt(volumenSonidos);
		dos.writeInt(dificultadSudoku);
		dos.writeBoolean(sudokuConAyuda);
		dos.writeLong(sudokuConTiempo);
	}
	public void cargar(InputStream is) throws
		IOException
		{
		DataInputStream dis = new DataInputStream(is);
		
		activarMusica = dis.readBoolean();
		volumenMusica = dis.readInt();
		activarSonidos = dis.readBoolean();
		volumenSonidos = dis.readInt();
		dificultadSudoku = dis.readInt();
		sudokuConAyuda = dis.readBoolean();
		sudokuConTiempo = dis.readLong();
	}
}