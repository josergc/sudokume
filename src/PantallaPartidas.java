import java.io.*;
import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.rms.*;

public class PantallaPartidas extends Lista implements CommandListener, Pantalla {
	public static final int CMD_NUEVO = 0;
	public static final int CMD_CARGAR = 1;
	public static final int CMD_ELIMINAR = 2;
	public static final int CMD_VOLVER = 3;
	protected GestionJuego gestionJuego;
	protected Command cmdNuevo;
	protected Command cmdCargar;
	protected Command cmdEliminar;
	protected Command cmdVolver;
	protected int ultimoComando;
	public PantallaPartidas(boolean conComandoNuevo) {
		super("Partidas guardadas",IMPLICIT);
	
		gestionJuego = SudokuME.gestionJuego;
		ultimoComando = CMD_VOLVER;
		
		cmdNuevo = new Command("Nuevo",Command.ITEM,1);
		if (conComandoNuevo) 
			addCommand(cmdNuevo);
		addCommand(cmdCargar = new Command("Cargar",Command.ITEM,1));
		addCommand(cmdEliminar = new Command("Eliminar",Command.ITEM,1));
		addCommand(cmdVolver = new Command("Volver",Command.ITEM,1));
		setCommandListener(this);
	}
	protected void actualizaLista() {
		try {
			Vector nombres = gestionJuego.listaPartidas();
			Enumeration e = nombres.elements();
			eliminaElementosLista();
			while (e.hasMoreElements()) 
				append((String)e.nextElement(),null);
		} catch (Exception e) {
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaError(e.toString())
				);
		}
	}
	public int devUltimoComando() {
		return ultimoComando;
	}
	public void commandAction(Command c, Displayable d) {
		if (c == SELECT_COMMAND || c == cmdCargar) {
			if (getSelectedIndex() != -1) {
				ultimoComando = CMD_CARGAR;
				SudokuME.nombrePartida = getString(getSelectedIndex());
				try {
					SudokuME.gestionJuego.cargarPartida(SudokuME.nombrePartida,SudokuME.sudoku);
					SudokuME.gestionPantallas.pantallaCerrada();
				} catch(Exception e) {
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaError("No se ha podido guardar la partida",e.toString())
						);
				}
			}
		} else if (c == cmdNuevo) {
			ultimoComando = CMD_NUEVO;
			SudokuME.gestionPantallas.ponPantalla(
				new PantallaGuardaPartida(SudokuME.nombrePartida)
				);
		} else if (c == cmdEliminar) {
			if (getSelectedIndex() != -1) {
				ultimoComando = CMD_ELIMINAR;
				try {
					SudokuME.gestionJuego.eliminarPartida(getString(getSelectedIndex()));
					actualizaLista();
				} catch (Exception e) {
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaError("No se ha podido eliminar la partida",e.toString())
						);
				}
			}
		} else if (c == cmdVolver) {
			ultimoComando = CMD_VOLVER;
			SudokuME.gestionPantallas.pantallaCerrada();
		}
	}
	public void activarPantalla() {
		actualizaLista();
	}
}