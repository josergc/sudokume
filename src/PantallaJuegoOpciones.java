/**
	Nombre del proyecto:
	Sudoku Microedition (SudokuME)

	Descripci�n de la clase:
	Esta clase representa la pantalla de opciones del juego durante la 
	partida. Podremos hacer que compruebe el tablero del juego con la 
	soluci�n calculada, guardar y cargar una partida, volver a la pantalla
	anterior, reiniciar la partida, configurar el juego o volver al men�
	principal.
	
	Autor: 
	Jos� Roberto Garc�a Chico
	josergc@lycos.es
	www.josergc.tk
	
	Hist�rico:
	1.0 - 29/05/2006
*/

import java.io.*;
import javax.microedition.lcdui.*;

public class PantallaJuegoOpciones extends Lista implements CommandListener, Pantalla {
	public static final int CMD_COMPROBAR_SUDOKU = 0;
	public static final int CMD_GUARDAR_SUDOKU = 1;
	public static final int CMD_CARGAR_SUDOKU = 2;
	public static final int CMD_VOLVER_SUDOKU = 3;
	public static final int CMD_REINICIA_SUDOKU = 4;
	public static final int CMD_CONFIGURAR_SUDOKU = 5;
	public static final int CMD_SALIR_SUDOKU = 6;
	protected int ultimoComando;
	protected static final Command cmdSeleccionar = new Command("Seleccionar",Command.ITEM,1);
	protected static final Command cmdAyuda = new Command("Ayuda de esta opci�n",Command.ITEM,1);
	protected boolean menuEntero;
	protected boolean pantallaNoActiva;
	protected static Image imgComprobar = null;
	protected static Image imgGuardar = null;
	protected static Image imgCargar = null;
	protected static Image imgVolver = null;
	protected static Image imgReinicia = null;
	protected static Image imgConfigurar = null;
	protected static Image imgSalir = null;
	
	public PantallaJuegoOpciones(boolean menuEntero) {
		super("Opciones",IMPLICIT);
		
		ultimoComando = CMD_VOLVER_SUDOKU;
		
		cargarImagenes();
		this.menuEntero = menuEntero;
		if (menuEntero)
			ponMenuEntero();
		else
			ponMenuMinimo();		
		pantallaNoActiva = true;
		
		addCommand(cmdSeleccionar);
		addCommand(cmdAyuda);
		setCommandListener(this);
	}
	
	protected void cargarImagenes() {
		if (imgSalir != null) 
			return;
		try {
			imgComprobar = Image.createImage("/comprobar.png");
			imgGuardar = Image.createImage("/guardar.png");
			imgCargar = Image.createImage("/cargar.png");
			imgConfigurar = Image.createImage("/configurar.png");
			imgReinicia = Image.createImage("/reinicia.png");
			imgVolver = Image.createImage("/volver.png");
			imgSalir = Image.createImage("/salir.png");
		} catch (IOException e) {
		}
	}
	
	protected void ponMenuEntero() {
		append("Comprobar Sudoku",imgComprobar);
		append("Guardar Sudoku",imgGuardar);
		ponMenuMinimo();
	}
	
	protected void ponMenuMinimo() {
		append("Cargar Sudoku",imgCargar);
		append("Volver al Sudoku",imgVolver);
		append("Reinicia partida",imgReinicia);
		append("Configurar Sudoku",imgConfigurar);
		append("Salir del Sudoku",imgSalir);
	}
	
	public int devUltimoComando() {
		return ultimoComando;
	}
	
	public void commandAction(Command c, Displayable d) {
		if (pantallaNoActiva)
			return;
		if (c == SELECT_COMMAND || c == cmdSeleccionar) {
			ultimoComando = menuEntero ? getSelectedIndex() : (getSelectedIndex() + CMD_CARGAR_SUDOKU);
			switch (ultimoComando) {
				case CMD_CONFIGURAR_SUDOKU:
					pantallaNoActiva = true;
					SudokuME.gestionPantallas.ponPantalla(SudokuME.pantallaConfiguracion);
				break;
				default:
					pantallaNoActiva = true;
					SudokuME.gestionPantallas.pantallaCerrada();
			} 
		} else if (c == cmdAyuda) {
			switch (menuEntero ? getSelectedIndex() : (getSelectedIndex() + CMD_CARGAR_SUDOKU)) {
				case CMD_COMPROBAR_SUDOKU:
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaMensaje("Comprobar sudoku","Con esta opci�n, permitimos que el m�vil compruebe las soluciones que hemos propuesto y las compara con la soluci�n que hab�a calulado poniendo de color rojo los n�meros que sean err�neos.\nTen en cuenta que al ejecutar esta funci�n, la partida se dar� como finalizada.")
						);
				break;
				case CMD_GUARDAR_SUDOKU:
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaMensaje("Guardar Sudoku","Con esta opci�n podremos guardar el estado actual de la partida.\nSe nos pedir� el nombre que le querados dar y tratar� de guardarla sobreescribiendo la que ya exista con el mismo nombre.")
						);
				break;
				case CMD_CARGAR_SUDOKU:
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaMensaje("Cargar Sudoku","Desde esta opci�n pasaremos a la pantalla de gesti�n de partidas donde podemos seleccionar la partida que queramos recuperar.")
						);
				break;
				case CMD_VOLVER_SUDOKU:
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaMensaje("Volver al Sudoku","Esta opci�n nos devuelve al tablero de juego.")
						);
				break;
				case CMD_REINICIA_SUDOKU:
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaMensaje("Reinicia partida","Utiliza esta opci�n si quieres reutilizar el tablero de juego como si de una partida nueva se tratara.")
						);
				break;
				case CMD_CONFIGURAR_SUDOKU:
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaMensaje("Configurar Sudoku","Configura el nivel de dificultad, si la partida se har� con tiempo y desactiva y activa tanto las melod�as como los sonidos e indica el volumen de estos.")
						);
				break;
				case CMD_SALIR_SUDOKU:
					SudokuME.gestionPantallas.ponPantalla(
						new PantallaMensaje("Salir del Sudoku","Vuelve al men� principal finalizando la partida actual.")
						);
				break;
			}
		}
	}
	
	public void activarPantalla() {
		pantallaNoActiva = false;
	}
}